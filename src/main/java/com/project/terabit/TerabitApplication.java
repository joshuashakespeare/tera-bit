package com.project.terabit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;



/**
 * The Class TerabitApplication.
 */

@EnableAutoConfiguration()
@SpringBootApplication
public class TerabitApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(TerabitApplication.class, args);
	}

}
