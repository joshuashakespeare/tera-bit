 package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.User;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.service.AdminService;


/**
 * The Class AdminController.
 * CRUD operations of ADMIN is handled
 */
@RestController
@RequestMapping(value="terabit/api/v1/admin")
public class AdminController {
	
	/** The admin service. */
	//*****AutoWiring: AdminService Object	# # #
	@Autowired							
	private AdminService adminService;
	

	
	/** The user repository. */
	//*****AutoWiring: UserRepository	# # #
	@Autowired							
	UserRepository userRepository;
	
	/** The log. */
	//*****Initializing:	log with the object of the current class(AdminController)	# # #
	Logger log=LoggerFactory.getLogger(this.getClass());		
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties property =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	

	
	private static final String RESPONSE="ERROR : ";
	

	
	/**
	 * Creates the admin.
	 *
	 * @param saltstring the saltstring
	 * @param userId the user id
	 * @return the user
	 * @throws ServiceException the service exception
	 * @throws ControllerException the controller exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	/**creating ADMIN for the seller  */
	@PostMapping(value= {"/save/","/save/{saltstring}"})
	public User createAdmin(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody User user) throws ServiceException,ControllerException,IOException{
		User newUser = new User();
		try {
			newUser = adminService.createAdmin(saltstring,user.getUserId());
			
		}catch(ControllerException|ServiceException exception) {
			
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			newUser.setMessage(s);
			log.error(exception.getMessage());
		}catch(Exception exception) {
			
			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(exception.getMessage());
			newUser.setMessage(s);
			log.error(s);
		}return newUser;
	}
	
	/**
	 * Delete admin.
	 *
	 * @param saltstring the saltstring
	 * @param userToBeDeleted the user to be deleted
	 * @return the user
	 * @throws ServiceException the service exception
	 * @throws ControllerException the controller exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	//Mapping the method for update
	/** deleting the existing ADMIN*/
	@PostMapping(value= {"/delete/","/delete/{saltstring}"})
	public User deleteAdmin(@PathVariable(value="saltstring")String saltstring,@RequestBody User userToBeDeleted) throws ServiceException,ControllerException,IOException
	{
		
		User userToBeReturned = new User();
		
		try {
			
			//*****Calling the delete method in AdminService
			userToBeReturned = adminService.deleteAdmin(userToBeDeleted,saltstring);
			
				
		}
		catch(ServiceException | ControllerException customException)
		{

			property.load(inputStream); 
			String s=RESPONSE+property.getProperty(customException.getMessage());
			userToBeReturned.setMessage(s);
			log.error(customException.getMessage());
			
		}
		//****** Catch Statement # # #
		catch(Exception exception)
		{
			//***** declaring the variable for Storing error message  # # #

			//*****Loading the content in inputstream to property # # #(
			property.load(inputStream);
			//***** setting the exception message from appilcation context
			String exceptionRecived=RESPONSE+property.getProperty(exception.getMessage());
			//***** setting the error message  to the user object
			userToBeReturned.setMessage(exceptionRecived);
			logg(exceptionRecived);
		}
		return userToBeReturned;
		
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}	
	
	
}
