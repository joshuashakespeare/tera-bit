package com.project.terabit.controller;


// TODO: Auto-generated Javadoc
/**
 * The Class ControllerException.
 */
public class ControllerException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new service exception as string.
	 *
	 * @param message the exception message ass string
	 */
	public ControllerException(String message) {
		super(message);
	}
}
