package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.User;
import com.project.terabit.service.DeleteUserServiceImpl;


/**
 * The Class DeleteController.
 */
@RestController
@RequestMapping(value="terabit/api/v1/user")
public class DeleteController {
	
	   /** The log. */
   	Logger log=LoggerFactory.getLogger(this.getClass());
       
       /** The prop. */
       Properties prop = new Properties(); 
       
       /** The input stream. */
       InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
       
       /** The Constant CONTROLLEREXCEPTION_SALTSTRINGMISSMATCH. */
        private static final String CONTROLLEREXCEPTION_URLMISSMATCH="DELETECONTROLLER.saltstring_not_provided";
   		private static final String CONTROLLEREXCEPTION_NOUSERID = "DELETECONTROLLER.no_userid_provided";

       
       /** The delete service. */
       @Autowired(required=true)
       private DeleteUserServiceImpl deleteService;
       
      
       
       /**
        * Delete user.
        *
        * @param saltstring the saltstring
        * @param user the user
        * @return the user
        * @throws ControllerException the controller exception
        * @throws IOException Signals that an I/O exception has occurred.
        */
       @PostMapping(value="/delete/{saltstring}")
       public User deleteUser(@PathVariable(name="saltstring", required=true) String saltstring, @RequestBody User user) throws ControllerException,IOException {
    	   		
              User newUser=new User();
              String response="";
              try {
            	if(user.getUserId()==null) {
  					throw new ControllerException(CONTROLLEREXCEPTION_NOUSERID);
  				}
  				else if(user.getSaltstring()==null){
  					throw new ControllerException(CONTROLLEREXCEPTION_URLMISSMATCH);
  				}else {
  					newUser=deleteService.deleteUser(saltstring,user);
  				}
              }catch(ControllerException|ServiceException exception) {
      			response="ERROR : ";
    			prop.load(inputStream); 
    			String s=response+prop.getProperty(exception.getMessage());
    			newUser.setMessage(s);
    			log.error(exception.getMessage());
    		}
              catch(Exception exception){
                     response="ERROR : ";
                     prop.load(inputStream); 
                     newUser.setMessage(response+prop.getProperty(exception.getMessage()));
                     String s=response+prop.getProperty(exception.getMessage());
                     log.error(s);      
              }return newUser;
       }
}

