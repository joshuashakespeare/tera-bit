package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Feedback;
import com.project.terabit.service.FeedBackService;
import com.project.terabit.service.ServiceException;


@CrossOrigin("*")
@RestController
@RequestMapping(value="/terabit/api/v1/feedback")
public class FeedBackController {

	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The property. */
	Properties property =new Properties();
	
	/** The input stream. */
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/** The feedback service. */
	@Autowired
	private FeedBackService feedbackService;
	
	/** Setting the value to be added before the exception*/
	private static final String RESPONSE = "ERROR :";
	
	private static final String CREATEFEEDBACKSUCCESSMESSAGE = "Feedback added successfully";
	
	private static final String UPDATEFEEDBACKSUCCESSMESSAGE = "Feedback updated successfully";

	
	
	@PostMapping(value= {"/add/","/add/{saltstring}"})
	public Feedback createFeedback(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Feedback feedback) throws ServiceException,ControllerException,IOException{
		
		Feedback newfeedback = new Feedback();
		try {
			property.load(inputStream); 
			
			//calling the create seller service
			newfeedback = feedbackService.createFeedback(feedback,saltstring);
			newfeedback.setMessage(CREATEFEEDBACKSUCCESSMESSAGE);
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			newfeedback.setMessage(string);
			log.error(exception.getMessage());
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			newfeedback.setMessage(string);
			log.error(string);
		}
		return newfeedback;
	}
	
	@PostMapping(value= {"/update/","/update/{saltstring}"})
	public Feedback updateFeedback(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody Feedback feedback) throws ServiceException,ControllerException,IOException{
		Feedback newfeedback = new Feedback();
		try {
			property.load(inputStream); 
			
			//calling the create seller service
			newfeedback = feedbackService.updateFeedback(feedback,saltstring);
			newfeedback.setMessage(UPDATEFEEDBACKSUCCESSMESSAGE);
		}
		catch(ControllerException|ServiceException exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			newfeedback.setMessage(string);
			log.error(exception.getMessage());
		}
		catch(Exception exception) 
		{
			property.load(inputStream); 
			String string=RESPONSE+property.getProperty(exception.getMessage());
			newfeedback.setMessage(string);
			log.error(string);
		}
		return newfeedback;
	}
}
