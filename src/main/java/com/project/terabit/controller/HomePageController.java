package com.project.terabit.controller;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Property;
import com.project.terabit.service.HomePagePropertyDisplayService;

/**
 * The Class HomePageController is used to display the homepage
 * of the product.
 */

@RestController
@RequestMapping(value="/terabit/api/v1/home")
@CrossOrigin("*")
public class HomePageController {
	
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired
	HomePagePropertyDisplayService homepagepropertydisplay;
	
	/** The prop. */
	Properties prop = new Properties(); 
	
	/** The input stream. */
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/**
	 * Homepage property.
	 *
	 * @param property the property
	 * @return the list
	 * @throws Exception the exception
	 */
	@PostMapping(value="/getproperty")
	public List<Property> homepageProperty(@RequestBody Property property) throws Exception {
		List<Property> propertylist=new ArrayList<>();
		String response=null;
		
		try {
			if(property.getPropertyId()==null) {
				
			propertylist=homepagepropertydisplay.propertyDisplay(BigInteger.valueOf(1));
			
			}else {
				propertylist=homepagepropertydisplay.propertyDisplay(property.getPropertyId());
			}
			
		}
		catch(Exception exception) {
			String string=response+prop.getProperty(exception.getMessage());
			log.error(string);
		}
		return propertylist;
	}

}
