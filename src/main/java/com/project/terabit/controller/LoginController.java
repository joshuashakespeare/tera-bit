package com.project.terabit.controller;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.User;
import com.project.terabit.service.LoginUserServiceImpl;
import com.project.terabit.service.ServiceException;





// TODO: Auto-generated Javadoc
/**
 * The Class LoginController.
 */

@RestController
@RequestMapping(value="/terabit/api/v1/user")
@CrossOrigin("*")
public class LoginController {
	
	/** The log. */
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	/** The loginservice. */
	@Autowired(required=true)
	private LoginUserServiceImpl loginservice;
	
	/** The prop. */
	Properties prop = new Properties(); 
	
	/** The input stream. */
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/**
	 * User login.
	 *
	 * @param userModel the user model
	 * @return the user
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@PostMapping(value="/login")
	public User userLogin(@RequestBody User userModel) throws ServiceException,IOException {
		
		User returnUserModel = new User();
		String response=null;
		try {
			
			returnUserModel=loginservice.login(userModel);
			response="Success";
			returnUserModel.setMessage(response);
		}catch(Exception exception) {
			returnUserModel=new User();
			response="Error";
			prop.load(inputStream); 
			returnUserModel.setMessage(response+prop.getProperty(exception.getMessage()));
			System.out.println(exception.getMessage());
			System.out.println(prop.getProperty(exception.getMessage()));
			String error=response+" "+prop.getProperty(exception.getMessage());
			returnUserModel.setMessage(error);
			log.error(error);
			
		}
		return returnUserModel;
		
	}
}
