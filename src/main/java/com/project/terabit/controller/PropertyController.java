package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.Property;
import com.project.terabit.model.User;
import com.project.terabit.service.PropertyService;
import com.project.terabit.service.ServiceException;


// TODO: Auto-generated Javadoc
/**
 * The Class AddPropertyBySellerController is used to add property for a seller.
 */
@RestController
@RequestMapping(value="terabit/api/v1/property")
@CrossOrigin("*")
public class PropertyController {
	
	   /** The log. */
   	Logger log=LoggerFactory.getLogger(this.getClass());
       
       /** The prop. */
       Properties prop = new Properties();
       
       /** The input stream. */
       InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
       
       /** The service. */
       @Autowired
       PropertyService service;
       
       
       /**
        * Addproperty by seller.
        *
        * @param saltstring the saltstring
        * @param userproperty the userproperty
        * @return the list<Property>
        * @throws ServiceException the service exception
        * @throws IOException Signals that an I/O exception has occurred.
        */
       @PostMapping(value= {"/addproperty/{saltstring}","/addproperty/"})
       public String addpropertyBySeller(@PathVariable(name="saltstring", required=false) String saltstring,@RequestBody AddPropertyUser userproperty ) throws  ServiceException,IOException {
    	  
    	   String response="";
    	   try {
    		   
    		   if(userproperty.getReturnPropertyId()==null) {
    		   response= service.addPropertyBySeller(userproperty,saltstring);
    		   }
    		   else{
    			   response=service.updatePropertyBySeller(userproperty, saltstring);
    		   }
    		   
    	   }catch(ServiceException exception) {
	   			prop.load(inputStream); 
	   			log.error(exception.getMessage());
    	   }catch(Exception exception){
               response="ERROR : ";
               prop.load(inputStream); 
               String error=response+prop.getProperty(exception.getMessage());
               log.error(error);      
        }
		return response;
       }
       
       
       @PostMapping(value= {"/getproperty/{saltstring}","/getproperty/"})
       public List<Property> getpropertyBySeller(@PathVariable(name="saltstring", required=false) String saltstring,@RequestBody User user ) throws  ServiceException,IOException {
		
    	   String response="";
    	   List<Property>  propertyList=new ArrayList<>();
    	   try {
    		   propertyList=service.getPropertyBySeller(user,saltstring);
    	   }catch(ServiceException exception) {
	   			prop.load(inputStream); 
	   			log.error(exception.getMessage());
   	   }catch(Exception exception){
              response="ERROR : ";
              prop.load(inputStream); 
              String error=response+prop.getProperty(exception.getMessage());
              log.error(error);      
       }
		return propertyList;
       
       }
       
       
       @DeleteMapping(value= {"/deleteproperty/{saltstring}","/deleteproperty/"})
       public String deletepropertyBySeller(@PathVariable(name="saltstring", required=false) String saltstring,@RequestBody AddPropertyUser addPropertyUserproperty ) throws  ServiceException,IOException {
    	   String response="";
    	   try {
    		   response=service.deletePropertyBySeller(addPropertyUserproperty,saltstring);
    	   }catch(ServiceException exception) {
	   			prop.load(inputStream); 
	   			log.error(exception.getMessage());
   	   }catch(Exception exception){
              response="ERROR : ";
              prop.load(inputStream); 
              String error=response+prop.getProperty(exception.getMessage());
              log.error(error);      
       }
		return response;
       
       }
    	         


}



