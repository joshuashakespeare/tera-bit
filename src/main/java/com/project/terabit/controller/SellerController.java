package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.CreateSeller;
import com.project.terabit.model.SaveSeller;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;
import com.project.terabit.service.SellerService;
import com.project.terabit.service.ServiceException;





/**
 * The Class SellerController.
 * The CRUD operations for seller is handled
 */
@RestController
@RequestMapping(value="/terabit/api/v1/seller")
public class SellerController {
	
		/** The seller service. */
		@Autowired
		private SellerService sellerService;
		

		
		
		/** The log. */
		//***** Declaring the logger for looging the information # # #
		Logger log=LoggerFactory.getLogger(this.getClass());
		
		/** The property. */
		Properties property =new Properties();
		
		/** The input stream. */
		InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");
		
		/** Setting the value to be added before the exception*/
		private static final String RESPONSE = "ERROR :";
		
		/** The Constant CREATESELLERSERVICE_SUCCESSMESSAGE. */
		private static final String CREATESELLERSERVICE_SUCCESSMESSAGE = "CREATESELLERSERVICE.Seller_created_successfully";
			
		
		private static final String SELLERUPDATE_SUCCESSMESSAGE = "SELLER_UPDATED_SUCCESSFULLY";

		/** The Constant SELLERDELETION_SUCCESSMESSAGE. */
		private static final String SELLERDELETION_SUCCESSMESSAGE="DELETESELLERSERVICE.seller_deleted_Successfully";
		
		/**
		 * Creates the seller.
		 *
		 * @param saltstring the saltstring
		 * @param createseller the createseller
		 * @return the seller
		 * @throws ServiceException the service exception
		 * @throws ControllerException the controller exception
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		/** Method to create seller*/
		@PostMapping(value= {"/save/","/save/{saltstring}"})
		public Seller createSeller(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody CreateSeller createseller) throws ServiceException,ControllerException,IOException{
			Seller seller = new Seller();
			try {
				property.load(inputStream); 
				
				//calling the create seller service
				seller = sellerService.createSeller(saltstring, createseller);
				seller.setMessage(CREATESELLERSERVICE_SUCCESSMESSAGE);
			}
			catch(ControllerException|ServiceException exception) 
			{
				property.load(inputStream); 
				String string=RESPONSE+property.getProperty(exception.getMessage());
				seller.setMessage(string);
				log.error(exception.getMessage());
			}
			catch(Exception exception) 
			{
				property.load(inputStream); 
				String string=RESPONSE+property.getProperty(exception.getMessage());
				seller.setMessage(string);
				log.error(string);
			}
			return seller;
		}
	
		
		
		/**
		 * Update seller.
		 *
		 * @param saltstring the saltstring
		 * @param updateToSeller the update to seller
		 * @return the seller
		 * @throws ControllerException the controller exception
		 * @throws Exception the exception
		 */
		/** Method to update the existing seller*/
		@PostMapping(value= {"/update/","/update/{saltstring}"})
		public Seller updateSeller(@PathVariable(name="saltstring") String saltstring,@RequestBody SaveSeller updateToSeller) throws ControllerException,Exception
		{
			Seller sellerUpdated=new Seller();
			
			try {
				 property.load(inputStream);
				 sellerUpdated= sellerService.updateSeller(updateToSeller,saltstring);
				sellerUpdated.setMessage(SELLERUPDATE_SUCCESSMESSAGE);
			}
			catch(ServiceException |ControllerException customException)
			{
				property.load(inputStream); 
				sellerUpdated.setMessage(RESPONSE+property.getProperty(customException.getMessage()));
				logg(RESPONSE+property.getProperty(customException.getMessage()));
			}
			
			catch(Exception exception)
			{
				property.load(inputStream); 
				String exceptionReceived=RESPONSE+property.getProperty(exception.getMessage());
				sellerUpdated.setMessage(exceptionReceived);
				logg(exceptionReceived);			
			}
			return sellerUpdated;
			
			
		}
		
		//***** Mapping to the the function with user object and saltString  # # # 
		
		/**
		 * Delete seller.
		 *
		 * @param saltstring the saltstring
		 * @param sellerUserToBeDeleted the seller user to be deleted
		 * @return the user
		 * @throws Exception the exception
		 */
		
		/** Deleting the existing SELLER*/
		@PostMapping(value= {"/delete/","/delete/{saltstring}"})
		public User deleteSeller(@PathVariable(name="saltstring") String saltstring,@RequestBody User sellerUserToBeDeleted) throws Exception
		{
			//***** starting try catch # # #
			User user = new User();
			try 
			{	
				property.load(inputStream);
				//***** calling deleteSeller from Service # # #
				 user = sellerService.deleteSeller(sellerUserToBeDeleted,saltstring);
				 user.setMessage(SELLERDELETION_SUCCESSMESSAGE);
			}
			catch(ServiceException |ControllerException customException)
			{
				property.load(inputStream); 
				String exceptionRecived=property.getProperty(customException.getMessage());		
				sellerUserToBeDeleted.setMessage(property.getProperty(customException.getMessage()));
				logg(exceptionRecived);
				
			}
			
			catch(Exception exception)
			{
				property.load(inputStream); 
				String response="ERROR:";
				String exceptionRecived=response+property.getProperty(exception.getMessage());	
				sellerUserToBeDeleted.setMessage(response+property.getProperty(exception.getMessage()));
				logg(exceptionRecived);
				
			}
			return user;
			
		}
		
		/**
		 * Logg.
		 *
		 * @param message the message
		 */
		private void logg(String message) {
			log.error(message);
		}	
		
	
	
}
