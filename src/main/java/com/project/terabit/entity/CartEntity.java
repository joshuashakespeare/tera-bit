package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;



// TODO: Auto-generated Javadoc
/**
 * The Class CartEntity.
 */
@Entity
@Table(name="cart")
@GenericGenerator(name="pkgenCartEntity",strategy="increment")
public class CartEntity {
	
	/** The cart id. */
	@Id
	@GeneratedValue(generator="pkgenCartEntity")
	@NotNull
	@Column(name="cart_id")
	private BigInteger cartId;
	
	/** The cart property id. */
	
	@Column(name="cart_property_id")
	private BigInteger cartPropertyId;
	
	/** The cart user id. */
	
	@Column(name="cart_user_id")
	private UUID cartUserId;
	
	/** The cart created time. */
	@NotNull
	@Column(name="cart_created_time")
	private LocalDateTime cartCreatedTime;
	
	/** The cart is active. */
	@NotNull
	@Column(name="cart_is_active",columnDefinition="boolean default false")
	private boolean cartIsActive;
	
	
	@NotNull
	@Column(name="cart_modified_time")
	private LocalDateTime cartModifiedTime;

	public LocalDateTime getCartModifiedTime() {
		return cartModifiedTime;
	}

	public void setCartModifiedTime(LocalDateTime cartModifiedTime) {
		this.cartModifiedTime = cartModifiedTime;
	}

	/**
	 * Gets the cart id.
	 *
	 * @return the cart id
	 */
	public BigInteger getCartId() {
		return cartId;
	}

	/**
	 * Sets the cart id.
	 *
	 * @param cartId the new cart id
	 */
	public void setCartId(BigInteger cartId) {
		this.cartId = cartId;
	}

	/**
	 * Gets the cart property id.
	 *
	 * @return the cart property id
	 */
	public BigInteger getCartPropertyId() {
		return cartPropertyId;
	}

	/**
	 * Sets the cart property id.
	 *
	 * @param cartPropertyId the new cart property id
	 */
	public void setCartPropertyId(BigInteger cartPropertyId) {
		this.cartPropertyId = cartPropertyId;
	}


	/**
	 * Gets the cart user id.
	 *
	 * @return the cart user id
	 */

	public UUID getCartUserId() {
		return cartUserId;
	}


	public void setCartUserId(UUID cartUserId) {
		this.cartUserId = cartUserId;
	}

	/**
	 * Gets the cart created time.
	 *
	 * @return the cart created time
	 */
	public LocalDateTime getCartCreatedTime() {
		return cartCreatedTime;
	}

	/**
	 * Sets the cart created time.
	 *
	 * @param cartCreatedTime the new cart created time
	 */
	public void setCartCreatedTime(LocalDateTime cartCreatedTime) {
		this.cartCreatedTime = cartCreatedTime;
	}

	/**
	 * Checks if is cart is active.
	 *
	 * @return true, if is cart is active
	 */
	public boolean isCartIsActive() {
		return cartIsActive;
	}

	/**
	 * Sets the cart is active.
	 *
	 * @param cartIsActive the new cart is active
	 */
	public void setCartIsActive(boolean cartIsActive) {
		this.cartIsActive = cartIsActive;
	}
	
	

}
