package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;


// TODO: Auto-generated Javadoc
/**
 * The Class ImageEntity.
 */
@Entity
@Table(name="image")
@GenericGenerator(name="pkgenImageEntity",strategy="increment")

public class ImageEntity {
	
	/** The image id. */
	@Id
	@GeneratedValue(generator="pkgenImageEntity")
	@NotNull
	@Column(name="image_id")
	private BigInteger imageId;
	
	/** The image path. */
	@NotNull
	@Column(name="image_path")
	private String imagePath;
	
	/** The image description. */
	@Column(name="image_description")
	private String imageDescription;
	
	/** The image is active. */
	@NotNull
	@Column(name="image_is_active",columnDefinition="boolean default false")
	private boolean imageIsActive;
	
	/** The image created time. */
	@Column(name="image_created_at")
	@NotNull
	private LocalDateTime imageCreatedTime;

	/**
	 * Gets the image id.
	 *
	 * @return the image id
	 */
	public BigInteger getImageId() {
		return imageId;
	}

	/**
	 * Sets the image id.
	 *
	 * @param imageId the new image id
	 */
	public void setImageId(BigInteger imageId) {
		this.imageId = imageId;
	}

	/**
	 * Gets the image path.
	 *
	 * @return the image path
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * Sets the image path.
	 *
	 * @param imagePath the new image path
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * Gets the image description.
	 *
	 * @return the image description
	 */
	public String getImageDescription() {
		return imageDescription;
	}

	/**
	 * Sets the image description.
	 *
	 * @param imageDescription the new image description
	 */
	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}

	/**
	 * Checks if is image is active.
	 *
	 * @return true, if is image is active
	 */
	public boolean isImageIsActive() {
		return imageIsActive;
	}

	/**
	 * Sets the image is active.
	 *
	 * @param imageIsActive the new image is active
	 */
	public void setImageIsActive(boolean imageIsActive) {
		this.imageIsActive = imageIsActive;
	}

	/**
	 * Gets the image created time.
	 *
	 * @return the image created time
	 */
	public LocalDateTime getImageCreatedTime() {
		return imageCreatedTime;
	}

	/**
	 * Sets the image created time.
	 *
	 * @param imageCreatedTime the new image created time
	 */
	public void setImageCreatedTime(LocalDateTime imageCreatedTime) {
		this.imageCreatedTime = imageCreatedTime;
	}
	
	

}
