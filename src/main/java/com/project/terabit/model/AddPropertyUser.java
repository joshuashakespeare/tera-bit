package com.project.terabit.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


// TODO: Auto-generated Javadoc
/**
 * The Class AddPropertyUser.
 */
public class AddPropertyUser {
	
	/** The userid. */
	private UUID userid;
	
	/** The return property id. */
	private BigInteger returnPropertyId;

	/** The property type. */
	private String propertyType;

	/** The property owned by. */
	//owned by the seller=1
	//other guy land=1
	private int propertyOwnedBy;

	/** The property landmark. */
	private String propertyLandmark;

	/** The property city. */
	private String propertyCity;

	/** The property state. */
	private String propertyState;

	/** The property country. */
	private String propertyCountry;

	/** The property latitude. */
	private String propertyLatitude;

	/** The property longitude. */
	private String propertyLongitude;

	/** The property description. */
	private String propertyDescription;

	/** The property cent. */
	private BigInteger propertyCent;

	/** The property esteemated amount. */
	private String propertyEsteematedAmount;

	/** The property image ids. */
	private List<Image> propertyImageIds = new ArrayList<>();
	
	/** The message. */
	private String message;

	public UUID getUserid() {
		return userid;
	}

	public void setUserid(UUID userid) {
		this.userid = userid;
	}

	public BigInteger getReturnPropertyId() {
		return returnPropertyId;
	}

	public void setReturnPropertyId(BigInteger returnPropertyId) {
		this.returnPropertyId = returnPropertyId;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public int getPropertyOwnedBy() {
		return propertyOwnedBy;
	}

	public void setPropertyOwnedBy(int propertyOwnedBy) {
		this.propertyOwnedBy = propertyOwnedBy;
	}

	public String getPropertyLandmark() {
		return propertyLandmark;
	}

	public void setPropertyLandmark(String propertyLandmark) {
		this.propertyLandmark = propertyLandmark;
	}

	public String getPropertyCity() {
		return propertyCity;
	}

	public void setPropertyCity(String propertyCity) {
		this.propertyCity = propertyCity;
	}

	public String getPropertyState() {
		return propertyState;
	}

	public void setPropertyState(String propertyState) {
		this.propertyState = propertyState;
	}

	public String getPropertyCountry() {
		return propertyCountry;
	}

	public void setPropertyCountry(String propertyCountry) {
		this.propertyCountry = propertyCountry;
	}

	public String getPropertyLatitude() {
		return propertyLatitude;
	}

	public void setPropertyLatitude(String propertyLatitude) {
		this.propertyLatitude = propertyLatitude;
	}

	public String getPropertyLongitude() {
		return propertyLongitude;
	}

	public void setPropertyLongitude(String propertyLongitude) {
		this.propertyLongitude = propertyLongitude;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public BigInteger getPropertyCent() {
		return propertyCent;
	}

	public void setPropertyCent(BigInteger propertyCent) {
		this.propertyCent = propertyCent;
	}

	public String getPropertyEsteematedAmount() {
		return propertyEsteematedAmount;
	}

	public void setPropertyEsteematedAmount(String propertyEsteematedAmount) {
		this.propertyEsteematedAmount = propertyEsteematedAmount;
	}

	public List<Image> getPropertyImageIds() {
		return propertyImageIds;
	}

	public void setPropertyImageIds(List<Image> propertyImageIds) {
		this.propertyImageIds = propertyImageIds;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
