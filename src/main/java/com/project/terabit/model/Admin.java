package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * The Class Admin.
 */
public class Admin {

	/** The admin id. */
	private BigInteger adminId;

	/** The admin seller id. */
	private List<Seller> adminSellerIds=new ArrayList<>();

	/** The admin rights by. */
	private BigInteger adminRightsBy;

	/** The admin is active. */
	private boolean adminIsActive;

	/** The admin created time. */
	private LocalDateTime adminCreatedTime;

	/** The admin seller count. */
	private BigInteger adminSellerCount;	

	/** The admin notification ids. */
	private List<Notification> adminNotificationIds = new ArrayList<>();

	/** The admin feedback ids. */
	private List<Feedback> adminFeedbackIds = new ArrayList<>();
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the admin id.
	 *
	 * @return the admin id
	 */
	public BigInteger getAdminId() {
		return adminId;
	}

	/**
	 * Sets the admin id.
	 *
	 * @param adminId the new admin id
	 */
	public void setAdminId(BigInteger adminId) {
		this.adminId = adminId;
	}

	/**
	 * Gets the admin seller id.
	 *
	 * @return the admin seller id
	 */
	

	/**
	 * Gets the admin rights by.
	 *
	 * @return the admin rights by
	 */
	public BigInteger getAdminRightsBy() {
		return adminRightsBy;
	}

	public List<Seller> getAdminSellerIds() {
		return adminSellerIds;
	}

	public void setAdminSellerIds(List<Seller> adminSellerIds) {
		this.adminSellerIds = adminSellerIds;
	}

	/**
	 * Sets the admin rights by.
	 *
	 * @param adminRightsBy the new admin rights by
	 */
	public void setAdminRightsBy(BigInteger adminRightsBy) {
		this.adminRightsBy = adminRightsBy;
	}

	/**
	 * Checks if is admin is active.
	 *
	 * @return true, if is admin is active
	 */
	public boolean isAdminIsActive() {
		return adminIsActive;
	}

	/**
	 * Sets the admin is active.
	 *
	 * @param adminIsActive the new admin is active
	 */
	public void setAdminIsActive(boolean adminIsActive) {
		this.adminIsActive = adminIsActive;
	}

	/**
	 * Gets the admin created time.
	 *
	 * @return the admin created time
	 */
	public LocalDateTime getAdminCreatedTime() {
		return adminCreatedTime;
	}

	/**
	 * Sets the admin created time.
	 *
	 * @param adminCreatedTime the new admin created time
	 */
	public void setAdminCreatedTime(LocalDateTime adminCreatedTime) {
		this.adminCreatedTime = adminCreatedTime;
	}

	/**
	 * Gets the admin seller count.
	 *
	 * @return the admin seller count
	 */
	public BigInteger getAdminSellerCount() {
		return adminSellerCount;
	}

	/**
	 * Sets the admin seller count.
	 *
	 * @param adminSellerCount the new admin seller count
	 */
	public void setAdminSellerCount(BigInteger adminSellerCount) {
		this.adminSellerCount = adminSellerCount;
	}

	/**
	 * Gets the admin notification ids.
	 *
	 * @return the admin notification ids
	 */
	public List<Notification> getAdminNotificationIds() {
		return adminNotificationIds;
	}

	/**
	 * Sets the admin notification ids.
	 *
	 * @param adminNotificationIds the new admin notification ids
	 */
	public void setAdminNotificationIds(List<Notification> adminNotificationIds) {
		this.adminNotificationIds = adminNotificationIds;
	}

	/**
	 * Gets the admin feedback ids.
	 *
	 * @return the admin feedback ids
	 */
	public List<Feedback> getAdminFeedbackIds() {
		return adminFeedbackIds;
	}

	/**
	 * Sets the admin feedback ids.
	 *
	 * @param adminFeedbackIds the new admin feedback ids
	 */
	public void setAdminFeedbackIds(List<Feedback> adminFeedbackIds) {
		this.adminFeedbackIds = adminFeedbackIds;
	}
	
}