package com.project.terabit.model;

import java.math.BigInteger;
import java.util.UUID;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateSeller.
 */
public class CreateSeller {
	
	/** The user id. */
	private UUID userId;
	
	/** The seller private contact. */
	private BigInteger sellerPrivateContact;

	/** The seller company name. */
	private String sellerCompanyName;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public UUID getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	/**
	 * Gets the seller private contact.
	 *
	 * @return the seller private contact
	 */
	public BigInteger getSellerPrivateContact() {
		return sellerPrivateContact;
	}

	/**
	 * Sets the seller private contact.
	 *
	 * @param sellerPrivateContact the new seller private contact
	 */
	public void setSellerPrivateContact(BigInteger sellerPrivateContact) {
		this.sellerPrivateContact = sellerPrivateContact;
	}

	/**
	 * Gets the seller company name.
	 *
	 * @return the seller company name
	 */
	public String getSellerCompanyName() {
		return sellerCompanyName;
	}

	/**
	 * Sets the seller company name.
	 *
	 * @param sellerCompanyName the new seller company name
	 */
	public void setSellerCompanyName(String sellerCompanyName) {
		this.sellerCompanyName = sellerCompanyName;
	}

	


}
