package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class Seller.
 */
public class Seller {
	
	
	

	/** The seller id. */
	private BigInteger sellerId;

	/** The seller property ids. */
	private List<Property> sellerPropertyIds = new ArrayList<>();

	/** The seller right by. */
	private BigInteger sellerRightBy;

	/** The seller is active. */
	private boolean sellerIsActive;

	/** The seller notification ids. */
	private List<Notification> sellerNotificationIds = new ArrayList<>();

	/** The seller feedback ids. */
	private List<Feedback> sellerFeedbackIds = new ArrayList<>(); 

	/** The seller created time. */
	private LocalDateTime sellerCreatedTime;

	/** The seller private contact. */
	private BigInteger sellerPrivateContact;

	/** The seller company name. */
	private String sellerCompanyName;

	/** The seller company logo id. */
	private Image sellerCompanyLogoId;

	/** The seller property count. */
	private BigInteger sellerPropertyCount;

	private String message;
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the seller id.
	 *
	 * @return the seller id
	 */
	public BigInteger getSellerId() {
		return sellerId;
	}

	/**
	 * Sets the seller id.
	 *
	 * @param sellerId the new seller id
	 */
	public void setSellerId(BigInteger sellerId) {
		this.sellerId = sellerId;
	}

	/**
	 * Gets the seller property ids.
	 *
	 * @return the seller property ids
	 */
	public List<Property> getSellerPropertyIds() {
		return sellerPropertyIds;
	}

	/**
	 * Sets the seller property ids.
	 *
	 * @param sellerPropertyIds the new seller property ids
	 */
	public void setSellerPropertyIds(List<Property> sellerPropertyIds) {
		this.sellerPropertyIds = sellerPropertyIds;
	}

	/**
	 * Gets the seller right by.
	 *
	 * @return the seller right by
	 */
	public BigInteger getSellerRightBy() {
		return sellerRightBy;
	}

	/**
	 * Sets the seller right by.
	 *
	 * @param sellerRightBy the new seller right by
	 */
	public void setSellerRightBy(BigInteger sellerRightBy) {
		this.sellerRightBy = sellerRightBy;
	}

	/**
	 * Checks if is seller is active.
	 *
	 * @return true, if is seller is active
	 */
	public boolean isSellerIsActive() {
		return sellerIsActive;
	}

	/**
	 * Sets the seller is active.
	 *
	 * @param sellerIsActive the new seller is active
	 */
	public void setSellerIsActive(boolean sellerIsActive) {
		this.sellerIsActive = sellerIsActive;
	}

	/**
	 * Gets the seller notification ids.
	 *
	 * @return the seller notification ids
	 */
	public List<Notification> getSellerNotificationIds() {
		return sellerNotificationIds;
	}

	/**
	 * Sets the seller notification ids.
	 *
	 * @param sellerNotificationIds the new seller notification ids
	 */
	public void setSellerNotificationIds(List<Notification> sellerNotificationIds) {
		this.sellerNotificationIds = sellerNotificationIds;
	}

	/**
	 * Gets the seller feedback ids.
	 *
	 * @return the seller feedback ids
	 */
	public List<Feedback> getSellerFeedbackIds() {
		return sellerFeedbackIds;
	}

	/**
	 * Sets the seller feedback ids.
	 *
	 * @param sellerFeedbackIds the new seller feedback ids
	 */
	public void setSellerFeedbackIds(List<Feedback> sellerFeedbackIds) {
		this.sellerFeedbackIds = sellerFeedbackIds;
	}

	/**
	 * Gets the seller created time.
	 *
	 * @return the seller created time
	 */
	public LocalDateTime getSellerCreatedTime() {
		return sellerCreatedTime;
	}

	/**
	 * Sets the seller created time.
	 *
	 * @param sellerCreatedTime the new seller created time
	 */
	public void setSellerCreatedTime(LocalDateTime sellerCreatedTime) {
		this.sellerCreatedTime = sellerCreatedTime;
	}

	/**
	 * Gets the seller private contact.
	 *
	 * @return the seller private contact
	 */
	public BigInteger getSellerPrivateContact() {
		return sellerPrivateContact;
	}

	/**
	 * Sets the seller private contact.
	 *
	 * @param sellerPrivateContact the new seller private contact
	 */
	public void setSellerPrivateContact(BigInteger sellerPrivateContact) {
		this.sellerPrivateContact = sellerPrivateContact;
	}

	/**
	 * Gets the seller company name.
	 *
	 * @return the seller company name
	 */
	public String getSellerCompanyName() {
		return sellerCompanyName;
	}

	/**
	 * Sets the seller company name.
	 *
	 * @param sellerCompanyName the new seller company name
	 */
	public void setSellerCompanyName(String sellerCompanyName) {
		this.sellerCompanyName = sellerCompanyName;
	}

	/**
	 * Gets the seller company logo id.
	 *
	 * @return the seller company logo id
	 */
	public Image getSellerCompanyLogoId() {
		return sellerCompanyLogoId;
	}

	/**
	 * Sets the seller company logo id.
	 *
	 * @param sellerCompanyLogoId the new seller company logo id
	 */
	public void setSellerCompanyLogoId(Image sellerCompanyLogoId) {
		this.sellerCompanyLogoId = sellerCompanyLogoId;
	}

	/**
	 * Gets the seller property count.
	 *
	 * @return the seller property count
	 */
	public BigInteger getSellerPropertyCount() {
		return sellerPropertyCount;
	}

	/**
	 * Sets the seller property count.
	 *
	 * @param sellerPropertyCount the new seller property count
	 */
	public void setSellerPropertyCount(BigInteger sellerPropertyCount) {
		this.sellerPropertyCount = sellerPropertyCount;
	}

	
	
}
