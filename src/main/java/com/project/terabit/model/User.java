package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;



// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
public class User {

	/** The user id. */
	private UUID userId;

	/** The user first name. */
	private String userFirstName;

	/** The user last name. */
	private String userLastName;

	/** The user password. */
	private String userPassword;

	/** The user email id. */
	private String userEmailId;

	/** The user contact no. */
	private BigInteger userContactNo;

	/** The user verifed. */
	private boolean userVerifed;

	/** The user authenticated. */
	private boolean userAuthenticated;
	
	/** The saltstring. */
	private String saltstring;

	/** The user pin. */
	private int userPin;

	/** The user is seller. */
	private boolean userIsSeller;

	/** The user is active. */
	private boolean userIsActive;

	/** The user created time. */
	private LocalDateTime userCreatedTime;

	/** The user modified time. */
	private LocalDateTime userModifiedTime;

	/** The user carts. */
	private List<Cart> userCarts = new ArrayList<>();

	/** The user seller id. */
	private Seller userSellerId;

	/** The user admin id. */
	private Admin userAdminId;

	/** The user feedbacks. */
	private List<Feedback> userFeedbacks = new ArrayList<>();

	/** The user notification ids. */
	private List<Notification> userNotificationIds = new ArrayList<>(); 

	/** The user viewed properties. */
	private List<ViewedProperty> userViewedProperties = new ArrayList<>();

	/** The user image. */
	private Image userImage;
	
	/** The message. */
	private String message;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public UUID getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user first name.
	 *
	 * @return the user first name
	 */
	public String getUserFirstName() {
		return userFirstName;
	}

	/**
	 * Sets the user first name.
	 *
	 * @param userFirstName the new user first name
	 */
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	/**
	 * Gets the user last name.
	 *
	 * @return the user last name
	 */
	public String getUserLastName() {
		return userLastName;
	}

	/**
	 * Sets the user last name.
	 *
	 * @param userLastName the new user last name
	 */
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	/**
	 * Gets the user password.
	 *
	 * @return the user password
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Sets the user password.
	 *
	 * @param userPassword the new user password
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * Gets the user email id.
	 *
	 * @return the user email id
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * Sets the user email id.
	 *
	 * @param userEmailId the new user email id
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * Gets the user contact no.
	 *
	 * @return the user contact no
	 */
	public BigInteger getUserContactNo() {
		return userContactNo;
	}

	/**
	 * Sets the user contact no.
	 *
	 * @param userContactNo the new user contact no
	 */
	public void setUserContactNo(BigInteger userContactNo) {
		this.userContactNo = userContactNo;
	}

	/**
	 * Checks if is user verifed.
	 *
	 * @return true, if is user verifed
	 */
	public boolean isUserVerifed() {
		return userVerifed;
	}

	/**
	 * Sets the user verifed.
	 *
	 * @param userVerifed the new user verifed
	 */
	public void setUserVerifed(boolean userVerifed) {
		this.userVerifed = userVerifed;
	}

	/**
	 * Checks if is user authenticated.
	 *
	 * @return true, if is user authenticated
	 */
	public boolean isUserAuthenticated() {
		return userAuthenticated;
	}

	/**
	 * Sets the user authenticated.
	 *
	 * @param userAuthenticated the new user authenticated
	 */
	public void setUserAuthenticated(boolean userAuthenticated) {
		this.userAuthenticated = userAuthenticated;
	}
	

	/**
	 * Gets the saltstring.
	 *
	 * @return the saltstring
	 */
	public String getSaltstring() {
		return saltstring;
	}

	/**
	 * Sets the saltstring.
	 *
	 * @param saltstring the new saltstring
	 */
	public void setSaltstring(String saltstring) {
		this.saltstring = saltstring;
	}

	/**
	 * Gets the user pin.
	 *
	 * @return the user pin
	 */
	public int getUserPin() {
		return userPin;
	}

	/**
	 * Sets the user pin.
	 *
	 * @param userPin the new user pin
	 */
	public void setUserPin(int userPin) {
		this.userPin = userPin;
	}

	/**
	 * Checks if is user is seller.
	 *
	 * @return true, if is user is seller
	 */
	public boolean isUserIsSeller() {
		return userIsSeller;
	}

	/**
	 * Sets the user is seller.
	 *
	 * @param userIsSeller the new user is seller
	 */
	public void setUserIsSeller(boolean userIsSeller) {
		this.userIsSeller = userIsSeller;
	}

	/**
	 * Checks if is user is active.
	 *
	 * @return true, if is user is active
	 */
	public boolean isUserIsActive() {
		return userIsActive;
	}

	/**
	 * Sets the user is active.
	 *
	 * @param userIsActive the new user is active
	 */
	public void setUserIsActive(boolean userIsActive) {
		this.userIsActive = userIsActive;
	}

	/**
	 * Gets the user created time.
	 *
	 * @return the user created time
	 */
	public LocalDateTime getUserCreatedTime() {
		return userCreatedTime;
	}

	/**
	 * Sets the user created time.
	 *
	 * @param userCreatedTime the new user created time
	 */
	public void setUserCreatedTime(LocalDateTime userCreatedTime) {
		this.userCreatedTime = userCreatedTime;
	}

	/**
	 * Gets the user modified time.
	 *
	 * @return the user modified time
	 */
	public LocalDateTime getUserModifiedTime() {
		return userModifiedTime;
	}

	/**
	 * Sets the user modified time.
	 *
	 * @param userModifiedTime the new user modified time
	 */
	public void setUserModifiedTime(LocalDateTime userModifiedTime) {
		this.userModifiedTime = userModifiedTime;
	}

	/**
	 * Gets the user carts.
	 *
	 * @return the user carts
	 */
	public List<Cart> getUserCarts() {
		return userCarts;
	}

	/**
	 * Sets the user carts.
	 *
	 * @param userCarts the new user carts
	 */
	public void setUserCarts(List<Cart> userCarts) {
		this.userCarts = userCarts;
	}

	/**
	 * Gets the user seller id.
	 *
	 * @return the user seller id
	 */
	public Seller getUserSellerId() {
		return userSellerId;
	}

	/**
	 * Sets the user seller id.
	 *
	 * @param userSellerId the new user seller id
	 */
	public void setUserSellerId(Seller userSellerId) {
		this.userSellerId = userSellerId;
	}

	/**
	 * Gets the user admin id.
	 *
	 * @return the user admin id
	 */
	public Admin getUserAdminId() {
		return userAdminId;
	}

	/**
	 * Sets the user admin id.
	 *
	 * @param userAdminId the new user admin id
	 */
	public void setUserAdminId(Admin userAdminId) {
		this.userAdminId = userAdminId;
	}

	/**
	 * Gets the user feedbacks.
	 *
	 * @return the user feedbacks
	 */
	public List<Feedback> getUserFeedbacks() {
		return userFeedbacks;
	}

	/**
	 * Sets the user feedbacks.
	 *
	 * @param userFeedbacks the new user feedbacks
	 */
	public void setUserFeedbacks(List<Feedback> userFeedbacks) {
		this.userFeedbacks = userFeedbacks;
	}

	/**
	 * Gets the user notification ids.
	 *
	 * @return the user notification ids
	 */
	public List<Notification> getUserNotificationIds() {
		return userNotificationIds;
	}

	/**
	 * Sets the user notification ids.
	 *
	 * @param userNotificationIds the new user notification ids
	 */
	public void setUserNotificationIds(List<Notification> userNotificationIds) {
		this.userNotificationIds = userNotificationIds;
	}

	/**
	 * Gets the user viewed properties.
	 *
	 * @return the user viewed properties
	 */
	public List<ViewedProperty> getUserViewedProperties() {
		return userViewedProperties;
	}

	/**
	 * Sets the user viewed properties.
	 *
	 * @param userViewedProperties the new user viewed properties
	 */
	public void setUserViewedProperties(List<ViewedProperty> userViewedProperties) {
		this.userViewedProperties = userViewedProperties;
	}

	/**
	 * Gets the user image.
	 *
	 * @return the user image
	 */
	public Image getUserImage() {
		return userImage;
	}

	/**
	 * Sets the user image.
	 *
	 * @param userImage the new user image
	 */
	public void setUserImage(Image userImage) {
		this.userImage = userImage;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}