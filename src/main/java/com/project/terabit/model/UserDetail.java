package com.project.terabit.model;

import java.math.BigInteger;

public class UserDetail {
	
	private BigInteger incommingSellerId;
	
	private BigInteger incommingPropertyId;

	/** The user first name. */
	private String userFirstName;

	/** The user last name. */
	private String userLastName;


	/** The user email id. */
	private String userEmailId;

	/** The user contact no. */
	private BigInteger userContactNo;
	
	/** The user is active. */
	private boolean userIsActive;

	/** The user image. */
	private Image userImage;
	
	/** The message. */
	private String message;
	
	
	public BigInteger getIncommingSellerId() {
		return incommingSellerId;
	}

	public void setIncommingSellerId(BigInteger incommingSellerId) {
		this.incommingSellerId = incommingSellerId;
	}
	
	 
	public BigInteger getIncommingPropertyId() {
		return incommingPropertyId;
	}

	public void setIncommingPropertyId(BigInteger incommingPropertyId) {
		this.incommingPropertyId = incommingPropertyId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public BigInteger getUserContactNo() {
		return userContactNo;
	}

	public void setUserContactNo(BigInteger userContactNo) {
		this.userContactNo = userContactNo;
	}

	public boolean isUserIsActive() {
		return userIsActive;
	}

	public void setUserIsActive(boolean userIsActive) {
		this.userIsActive = userIsActive;
	}

	public Image getUserImage() {
		return userImage;
	}

	public void setUserImage(Image userImage) {
		this.userImage = userImage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
