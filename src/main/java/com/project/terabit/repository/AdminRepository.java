package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.SellerEntity;

@Repository
public interface AdminRepository extends CrudRepository<AdminEntity, BigInteger> {

	@Query(value = "SELECT * FROM seller s WHERE s.seller_right_by = :sellerRightBy", nativeQuery = true)
	List<SellerEntity> getSellerByAdmin(@Param(value = "sellerRightBy") BigInteger adminId); 
	
}
