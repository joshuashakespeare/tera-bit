package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.FeedbackEntity;

@Repository
public interface FeedBackRepository extends CrudRepository<FeedbackEntity, BigInteger>  {
	
	@Query(value = "SELECT * FROM feedback f WHERE f.user_id = :userId",nativeQuery=true)
	List<FeedbackEntity> findFeedbacksByUserId(@Param(value="userId") UUID userId);
	
	@Query(value = "SELECT * FROM feedback f WHERE f.feedback_id = :feedbackId",nativeQuery=true)
	FeedbackEntity findFeedbacksByFeedbackId(@Param(value="feedbackId") BigInteger feedbackId);
}