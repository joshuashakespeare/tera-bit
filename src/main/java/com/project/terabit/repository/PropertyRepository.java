package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.PropertyEntity;

@Repository
public interface PropertyRepository extends CrudRepository<PropertyEntity, BigInteger> {
	
	
	@Query(value="SELECT * FROM property p where p.property_id>:property_id and property_is_active=true order by p.property_id limit 6", nativeQuery=true)
	List<PropertyEntity> getpropertyForHomePage(@Param (value="property_id") BigInteger lastpropertyid);
	
	@Query(value="SELECT COUNT(*) FROM property", nativeQuery=true)
	BigInteger getTotalPropertyCount();
	
	@Query(value="SELECT * FROM property p where p.property_id>:property_id and property_is_active=true order by p.property_id", nativeQuery=true)
	List<PropertyEntity> getpropertyForHomePageLastfew(@Param (value="property_id") BigInteger lastpropertyid);
	
	@Query(value = "SELECT * FROM property p WHERE p.property_id = :property_id AND p.property_is_active = 't'", nativeQuery = true)
	PropertyEntity getPropertyForViewedProperty(@Param(value = "property_id") BigInteger propertyId);
	
	@Query(value ="SELECT * FROM property p WHERE p.property_id = :propertyId",nativeQuery=true)
	public PropertyEntity findPropertyByPropertyId(@Param(value="propertyId") BigInteger propertyId);
	
	@Query(value="SELECT * FROM property where property_id=:propertyId and property_is_active=true",nativeQuery=true)
	public PropertyEntity getPropertyByPropertyId(@Param(value="propertyId") BigInteger propertyId);
}
