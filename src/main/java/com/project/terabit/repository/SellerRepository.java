package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
@Repository
public interface SellerRepository extends CrudRepository<SellerEntity, BigInteger>{

	
	@Query(value = "SELECT * FROM seller s WHERE s.seller_private_contact = :contactNo", nativeQuery = true)
	public SellerEntity findSellerByContactNumber(@Param(value="contactNo") BigInteger contactNo);
	
	@Query(value = "SELECT * FROM property p WHERE p.property_owned_by = :sellerId", nativeQuery = true)
	public List<PropertyEntity> findPropertyBySeller(@Param(value = "sellerId") BigInteger sellerId);
	
	
}
