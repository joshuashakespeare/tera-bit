package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.UsersEntity;



/**
 * The Interface UserRepository.
 */
@Repository
public interface UserRepository extends CrudRepository<UsersEntity, UUID>  {
	
	/**
	 * Find user by email ID.
	 *
	 * @param emailId the email id
	 * @return the user entity
	 */
	@Query(value="SELECT * FROM users u  WHERE u.user_email_id LIKE :emailId", nativeQuery=true)
	UsersEntity findUserByEmailID(@Param(value="emailId") String emailId);
	
	/**
	 * Find user by contact number.
	 *
	 * @param contactNumber the contact number
	 * @return the user entity
	 */
	@Query(value="SELECT * FROM users u  WHERE u.user_contact_no = :contactNumber", nativeQuery=true)
	UsersEntity findUserByContactNumber(@Param(value="contactNumber") BigInteger contactNumber);
	
	/**
	 * Check user.
	 *
	 * @param mailId the mail id
	 * @param contactNumber the contact number
	 * @return the user entity
	 */
	@Query(value = "SELECT * FROM users u WHERE u.user_email_id like :mailId OR u.user_contact_no = :contactNumber", nativeQuery = true)
	public UsersEntity checkUser(@Param(value="mailId") String mailId, @Param(value="contactNumber") BigInteger contactNumber);
	
	/**
	 * Find user by user id.
	 *
	 * @param userId the user id
	 * @return the user entity
	 */
	@Query(value ="SELECT * FROM users u WHERE u.user_id = :userId",nativeQuery=true)
	public UsersEntity findUserByUserId(@Param(value="userId") UUID userId);
	
	@Query(value ="SELECT * FROM users u WHERE u.user_id = :userId AND u.user_is_active = 't' AND u.user_salt_string = :saltstring",nativeQuery=true)
	public UsersEntity findUserById(@Param(value="userId") UUID userId,@Param(value="saltstring") String saltstring);
	
	@Query(value ="SELECT * FROM users u WHERE u.user_id = :userId AND u.user_is_active = 't'",nativeQuery=true)
	public UsersEntity findActiveUsers(@Param(value="userId") UUID userId);
	
	/**
	 * Checking existing seller.
	 *
	 * @param userId the user id
	 * @return the users entity
	 */
	@Query(value = "SELECT * FROM users u WHERE u.user_id =:userId AND u.user_is_seller = false AND u.seller_id = null",nativeQuery = true)
	public UsersEntity checkingExistingSeller(@Param(value="userId") UUID userId); 
	
	
	@Query(value="SELECT user_salt_string FROM users  WHERE seller_id=:sellerId",nativeQuery=true)
	public String saltstringComparisonForSellerId(@Param(value="sellerId") BigInteger sellerId);
	
	@Query(value="select * from users u inner join viewed_property v on u.user_id=v.viewed_user_id where v.viewed_property_property_id=:propertyId",nativeQuery=true)
	public List<UsersEntity> userDetailsForViewedPropertyPropertyId(@Param(value="propertyId") BigInteger propertyId);
	
	
}
