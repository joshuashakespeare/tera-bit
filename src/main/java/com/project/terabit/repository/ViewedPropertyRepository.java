package com.project.terabit.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.ViewedPropertyEntity;

@Repository
public interface ViewedPropertyRepository extends CrudRepository<ViewedPropertyEntity, BigInteger>{

}

