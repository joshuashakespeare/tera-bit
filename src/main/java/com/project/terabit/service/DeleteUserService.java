package com.project.terabit.service;

import org.springframework.stereotype.Component;

import com.project.terabit.model.User;


/**
 * The Interface DeleteService.
 */
@Component
public interface DeleteUserService {

	
	/**
	 * Delete user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws Exception the exception
	 */
	public User deleteUser(String saltstring,User user) throws Exception;
}
