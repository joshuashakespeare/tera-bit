package com.project.terabit.service;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.controller.ControllerException;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.User;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.DeleteUserValidator;


/**
 * The Class DeleteServiceImpl.
 */
@Service
@Transactional(readOnly=true)
public class DeleteUserServiceImpl implements DeleteUserService{
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The user repository. */
	@Autowired
	private UserRepository userRepository;
	
	
	/** The Constant SERVICEEXCEPTION_SUCCESSMESSAGE. */
	private static final String SERVICEEXCEPTION_SUCCESSMESSAGE = "DELETEUSERSERVICE.user_deleted"; 
	
	/** The Constant SERVICEEXCEPTION_USERIDEXCEPTION. */
	private static final String SERVICEEXCEPTION_USERIDEXCEPTION = "DELETEUSERSERVICE.invalid_user";
	
	private static final String SERVICEEXCEPTION_SALTSTRINGMISSMATCH = "DELETEUSERSERVICE.invalid_saltstring";
	
    private static final String SERVICEEXCEPTION_URLMISSMATCH="DELETEUSERSERVICE.saltstring_not_provided";
	private static final String SERVICEEXCEPTION_NOUSERID = "DELETEUSERSERVICE.no_userid_provided";

	/* (non-Javadoc)
	 * @see com.project.terabit.service.DeleteService#deleteUser(com.project.terabit.model.User)
	 */
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User deleteUser(String saltstring,User user) throws Exception{
		System.out.println(saltstring);
		UsersEntity userEntity = new UsersEntity();
		try {
			if(user.getUserId()==null) {
				throw new ControllerException(SERVICEEXCEPTION_NOUSERID);
			}
			System.out.println(user.getUserId());
			if(saltstring==null){
				throw new ControllerException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			DeleteUserValidator.validate(user.getUserId().toString());
			userEntity = userRepository.findUserByUserId(user.getUserId());
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}else if(!userEntity.getSaltString().equals(saltstring)) {
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRINGMISSMATCH);
			}
			userEntity.setUserIsActive(false);
			userRepository.save(userEntity);
			user.setMessage(SERVICEEXCEPTION_SUCCESSMESSAGE);
			return user;
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("deleteUser "+exception.getMessage());
			throw exception;
		}
	}
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}


}
