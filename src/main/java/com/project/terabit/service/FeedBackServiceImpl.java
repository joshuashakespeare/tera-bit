package com.project.terabit.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Feedback;
import com.project.terabit.repository.FeedBackRepository;
import com.project.terabit.repository.UserRepository;


@Service
@Transactional(readOnly=true)
public class FeedBackServiceImpl implements FeedBackService{

	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	
	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	FeedBackRepository feedbackRepository;
	
	
	
	/** The Constant SERVICEEXCEPTION_NOSALTSTRING. */
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "UTILITYSERVICE.no_saltstring";	
	
	/** The Constant SERVICEEXCEPTION_NOUSERID. */
	private static final String SERVICEEXCEPTION_NOUSERID = "UTILITYSERVICE.invalid_user_id";
	
	/** The Constant SERVICEEXCEPTION_NOUSER. */
	private static final String SERVICEEXCEPTION_NOUSER = "UTILITYSERVICE.No_User_Exists";
	
	private static final String SERVICEEXCEPTION_NOFEEDBACK = "DELETEFEEDBACKSERVICE.No_feedback";
	

	
	@Override
	public Feedback createFeedback(Feedback feedback, String saltString) throws Exception {
		Feedback feedbackToBeReturned = new Feedback();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			if(feedback.getFeedbackGivenBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			UsersEntity userEntity = userRepository.findUserById(feedback.getFeedbackGivenBy(), saltString);
			
			if(userEntity==null)  {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}

			FeedbackEntity feedbackEntity = new FeedbackEntity();
			feedbackEntity.setFeedbackCreatedBy(userEntity.getUserFirstName()+" "+userEntity.getUserLastName());
			feedbackEntity.setFeedbackCreatedTime(LocalDateTime.now());
			feedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
			feedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
			feedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
			feedbackEntity.setFeedbackGivenBy(feedback.getFeedbackGivenBy());
			feedbackEntity = feedbackRepository.save(feedbackEntity);
			List<FeedbackEntity> feedbacksToBeAdded =  userEntity.getUserFeedbacks();
			feedbacksToBeAdded.add(feedbackEntity);
			userEntity.setUserFeedbacks(feedbacksToBeAdded);
			userRepository.save(userEntity);
			
			feedbackToBeReturned.setFeedbackId(feedbackEntity.getFeedbackId());
			feedbackToBeReturned.setFeedbackDescription(feedbackEntity.getFeedbackDescription());
			feedbackToBeReturned.setFeedbackGivenBy(userEntity.getUserId());
			feedbackToBeReturned.setFeedbackRating(feedbackEntity.getFeedbackRating());
			feedbackToBeReturned.setFeedbackCreatedBy(feedbackEntity.getFeedbackCreatedBy());
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return feedbackToBeReturned;
	}
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}


	@Override
	public Feedback updateFeedback(Feedback feedback, String saltString) throws Exception {
		Feedback feedbackToBeReturned = new Feedback();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			if(feedback.getFeedbackGivenBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			
			UsersEntity userEntity = userRepository.findUserById(feedback.getFeedbackGivenBy(), saltString);
			
			if(userEntity==null)  {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			FeedbackEntity feedbackEntity = feedbackRepository.findFeedbacksByFeedbackId(feedback.getFeedbackId());
			if(feedbackEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOFEEDBACK);
			}
			feedbackEntity.setFeedbackDescription(feedback.getFeedbackDescription());
			feedbackEntity.setFeedbackRating(feedback.getFeedbackRating());
			feedbackEntity.setFeedbackModifiedTime(LocalDateTime.now());
			feedbackRepository.save(feedbackEntity);
			
			feedbackToBeReturned.setFeedbackId(feedbackEntity.getFeedbackId());
			feedbackToBeReturned.setFeedbackDescription(feedbackEntity.getFeedbackDescription());
			feedbackToBeReturned.setFeedbackGivenBy(userEntity.getUserId());
			feedbackToBeReturned.setFeedbackRating(feedbackEntity.getFeedbackRating());
			feedbackToBeReturned.setFeedbackCreatedBy(feedbackEntity.getFeedbackCreatedBy());
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return feedbackToBeReturned;
	}


	@Override
	public List<Feedback> retriveFeedback(Feedback feedback, String saltString) throws Exception {
		Feedback feedbackToBeRetrived = new Feedback();
		List<Feedback> feedbackListToBeReturned = new ArrayList<>();
		try {
			if(saltString==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			if(feedback.getFeedbackGivenBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			
			UsersEntity userEntity = userRepository.findUserById(feedback.getFeedbackGivenBy(), saltString);
			
			if(userEntity==null)  {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			List<FeedbackEntity> feedbackEntity = feedbackRepository.findFeedbacksByUserId(feedback.getFeedbackGivenBy());
			if(feedbackEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOFEEDBACK);
			}
		
			for(int index=0;index<feedbackEntity.size();index++) {
				feedbackToBeRetrived.setFeedbackDescription(feedbackEntity.get(index).getFeedbackDescription());
				feedbackToBeRetrived.setFeedbackGivenBy(feedbackEntity.get(index).getFeedbackGivenBy());
				feedbackToBeRetrived.setFeedbackId(feedbackEntity.get(index).getFeedbackId());
				feedbackToBeRetrived.setFeedbackRating(feedbackEntity.get(index).getFeedbackRating());
				feedbackListToBeReturned.add(feedbackToBeRetrived);
			}
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}
				
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		return feedbackListToBeReturned;
	}

}
