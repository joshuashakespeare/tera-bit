package com.project.terabit.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.project.terabit.model.Property;

@Service
public interface HomePagePropertyDisplayService {
	
	public List<Property> propertyDisplay(BigInteger lastPropertyId) throws Exception;

}
