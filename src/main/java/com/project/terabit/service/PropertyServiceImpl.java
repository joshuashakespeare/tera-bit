package com.project.terabit.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.terabit.controller.ControllerException;
import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.entity.ViewedPropertyEntity;
import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.Feedback;
import com.project.terabit.model.Image;
import com.project.terabit.model.Property;
import com.project.terabit.model.User;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.AddPropertyValidator;


/**
 * The Class AddPropertyBySellerImpl.
 */
@Service
public class PropertyServiceImpl implements PropertyService {
	
	/** The userrepository. */
	@Autowired
	UserRepository userrepository;
	
	/** The propertyrepository. */
	@Autowired
	PropertyRepository propertyrepository;
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The Constant SERVICEXCEPTIONNOUSERID. */
	public static final String SERVICEXCEPTIONNOUSERID="SERVICE.no_user_id_present";
	
	/** The Constant SERVICEXCEPTIONUSERNOTSELLER. */
	public static final String SERVICEXCEPTIONUSERNOTSELLER="SERVICE.no_user_id_present";
	
	/** The Constant SUCCESSMESSAGE. */
	public static final String SUCCESSMESSAGEFORADD="ADDPROPERTY.success";
	
	public static final String SUCCESSMESSAGEFORDELETE="DELETEPROPERTY.success";
	
	public static final String SUCCESSMESSAGEFORUPDATE="UPDATEPROPERTY.success";
	
	/** The Constant SERVICEEXCEPTIONSALTSTRINGNOTPRESENT. */
	private static final String SERVICEEXCEPTIONSALTSTRINGNOTPRESENT = "PROPERTY.saltstringmissing";
	
	/** The Constant CONTROLLEREXCEPTION_NOUSERID. */
    private static final String SERVICEEXCEPTIONNOUSERID = "PROPERTY.nouserid";
    
    /** The Constant CONTROLLEREXCEPTION_URLMISSMATCH. */
    private static final String SERVICEEXCEPTIONURLMISSMATCH = "PROPERTY.saltstringmissmatch";
    
    /** The Constant CONTROLLEREXCEPTION_NOUSER. */
    private static final String SERVICEEXCEPTIONNOUSER = "PROPERTY.nouser";
    
    /** The Constant CONTROLLEREXCEPTION_NOUSER. */
    private static final String SERVICEEXCEPTIONNOSELLER = "PROPERTY.noseller";
    
    private static final String SERVICEEXCEPTIONPROPERYALREADYINACTIVE = "DELETEPROPERTYSERVICE.propertyalreadyinactive";
    
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.AddPropertyBySeller#addPropertyBySeller(com.project.terabit.model.AddPropertyUser, java.lang.String)
	 */
	public String addPropertyBySeller(AddPropertyUser userproperty,String saltstring) throws Exception {
		
		try {
			
			if(saltstring==null) {
  			   throw new ControllerException(SERVICEEXCEPTIONSALTSTRINGNOTPRESENT);
  		   }
			System.out.println(userproperty.getUserid());
  		   if(userproperty.getUserid()==null) {
  			   throw new ControllerException(SERVICEEXCEPTIONNOUSERID);
  		   }
  		 UsersEntity userentity=userrepository.findUserByUserId(userproperty.getUserid());
		   if(userentity==null) {
			   throw new ControllerException(SERVICEEXCEPTIONNOUSER);
		   }
		   if(!userentity.getSaltString().equals(saltstring)) {
			   throw new ControllerException(SERVICEEXCEPTIONURLMISSMATCH); 
		   }
			UUID userid=userproperty.getUserid();
			
			Property p=this.propertyFromAddPropety(userproperty);
			AddPropertyValidator.validate(p, userid);
			if( !userentity.isUserIsSeller()) {
				throw new ServiceException(SERVICEXCEPTIONUSERNOTSELLER);
			}
			else {
				PropertyEntity propertyentity=new PropertyEntity();
				propertyentity.setPropertyCent(p.getPropertyCent());
				propertyentity.setPropertyCity(p.getPropertyCity());
				propertyentity.setPropertyCountry(p.getPropertyCountry());
				propertyentity.setPropertyCreatedTime(LocalDateTime.now());
				propertyentity.setPropertyDescription(p.getPropertyDescription());
				propertyentity.setPropertyEsteematedAmount(p.getPropertyEsteematedAmount());
				propertyentity.setPropertyLandmark(p.getPropertyLandmark());
				propertyentity.setPropertyLatitude(p.getPropertyLatitude());
				propertyentity.setPropertyLongitude(p.getPropertyLongitude());
				propertyentity.setPropertyModifiedTime(LocalDateTime.now());
				propertyentity.setPropertyOwnedBy(p.getPropertyOwnedBy());
				propertyentity.setPropertyState(p.getPropertyState());
				propertyentity.setPropertyType(p.getPropertyType());
				propertyentity.setPropertyIsActive(true);
				if(!p.getPropertyImageIds().isEmpty()) {
				propertyentity.setPropertyImageId(this.imageEntityForProperty(p.getPropertyImageIds()));
				}
				userentity.getUserSellerId().getSellerPropertyId().add(propertyentity);
				userrepository.save(userentity);
				return SUCCESSMESSAGEFORADD;
			}
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("AddPropertyBySeller "+exception.getMessage());
			throw exception;
		}
	}
	
	
	
public String updatePropertyBySeller(AddPropertyUser userproperty,String saltstring) throws Exception {
		
		try {
			List<PropertyEntity> propertyEntityListOfSeller;
			if(saltstring==null) {
  			   throw new ServiceException(SERVICEEXCEPTIONSALTSTRINGNOTPRESENT);
  		   }
  		   if(userproperty.getUserid()==null) {
  			   throw new ServiceException(SERVICEEXCEPTIONNOUSERID);
  		   }
  		 UsersEntity userentity=userrepository.findUserByUserId(userproperty.getUserid());
		   if(userentity==null) {
			   throw new ServiceException(SERVICEEXCEPTIONNOUSER);
		   }
		   if(!userentity.getSaltString().equals(saltstring)) {
			   throw new ServiceException(SERVICEEXCEPTIONURLMISSMATCH); 
		   }
		   UUID userid=userproperty.getUserid();
			
			Property p=this.propertyFromAddPropety(userproperty);
			AddPropertyValidator.validate(p, userid);
			if( !userentity.isUserIsSeller()) {
				throw new ServiceException(SERVICEXCEPTIONUSERNOTSELLER);
			}else {
				propertyEntityListOfSeller=userentity.getUserSellerId().getSellerPropertyId();
				for(Integer i=0;i<propertyEntityListOfSeller.size();i++) {
					PropertyEntity PropertyToBeUpdated=propertyEntityListOfSeller.get(i);
					if(userproperty.getReturnPropertyId()==PropertyToBeUpdated.getPropertyId()) {
						PropertyToBeUpdated.setPropertyCent(p.getPropertyCent());
						PropertyToBeUpdated.setPropertyCity(p.getPropertyCity());
						PropertyToBeUpdated.setPropertyCountry(p.getPropertyCountry());
						PropertyToBeUpdated.setPropertyDescription(p.getPropertyDescription());
						PropertyToBeUpdated.setPropertyEsteematedAmount(p.getPropertyEsteematedAmount());
						PropertyToBeUpdated.setPropertyLandmark(p.getPropertyLandmark());
						PropertyToBeUpdated.setPropertyLatitude(p.getPropertyLatitude());
						PropertyToBeUpdated.setPropertyLongitude(p.getPropertyLongitude());
						PropertyToBeUpdated.setPropertyModifiedTime(LocalDateTime.now());
						PropertyToBeUpdated.setPropertyOwnedBy(p.getPropertyOwnedBy());
						PropertyToBeUpdated.setPropertyState(p.getPropertyState());
						PropertyToBeUpdated.setPropertyType(p.getPropertyType());
						if(!p.getPropertyImageIds().isEmpty()) {
							PropertyToBeUpdated.setPropertyImageId(this.imageEntityForProperty(p.getPropertyImageIds()));
						}
						if(!p.getPropertyFeedbackIds().isEmpty()) {
							PropertyToBeUpdated.setPropertyFeedbackId(this.feedbackEntityList(p.getPropertyFeedbackIds()));
						}
						if(!p.getPropertyViewedIds().isEmpty()) {
							PropertyToBeUpdated.setPropertyViewedId(this.viewedpropertyEntityList(p.getPropertyViewedIds()));
						}
					}
				}
				
			}
			userrepository.save(userentity);
			return SUCCESSMESSAGEFORUPDATE;
			
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("AddPropertyBySeller "+exception.getMessage());
			throw exception;
		}
	}

public List<Property> getPropertyBySeller(User user,String saltstring) throws Exception {
	try {
		if(saltstring==null) {
			   throw new ServiceException(SERVICEEXCEPTIONSALTSTRINGNOTPRESENT);
		}
	   if(user.getUserId()==null) {
  		   throw new ControllerException(SERVICEEXCEPTIONNOUSERID);
  		}
	   UsersEntity userentity=userrepository.findUserByUserId(user.getUserId());
	   if(!userentity.getSaltString().equals(saltstring)) {
		   throw new ControllerException(SERVICEEXCEPTIONURLMISSMATCH); 
		}
	   if(userentity.getUserSellerId()==null) {
		   throw new ServiceException(SERVICEEXCEPTIONNOSELLER);
	   }
	   if( !userentity.isUserIsSeller()) {
	       throw new ServiceException(SERVICEXCEPTIONUSERNOTSELLER);
	   }
		   
	   List<PropertyEntity> propertyEntityList=userentity.getUserSellerId().getSellerPropertyId();
	   System.out.println(propertyEntityList.size());
	   List<Property> propertylist=this.returnproperty(propertyEntityList);
	   return propertylist;
	}catch(ServiceException exception) {
		logg(exception.getMessage());
		throw exception;
	}catch(Exception exception) {
		logg("AddPropertyBySeller "+exception.getMessage());
		throw exception;
	}
	
}

public String deletePropertyBySeller(AddPropertyUser userProperty,String saltstring) throws Exception {
	

	try {
		
		if(saltstring==null) {
			   throw new ControllerException(SERVICEEXCEPTIONSALTSTRINGNOTPRESENT);
		 }
	    if(userProperty.getUserid()==null) {
	    	   throw new ControllerException(SERVICEEXCEPTIONNOUSERID);
		 }
		UsersEntity userentity=userrepository.findUserByUserId(userProperty.getUserid());
	    if(userentity==null) {
		       throw new ControllerException(SERVICEEXCEPTIONNOUSER);
	     }
	    if(!userentity.getSaltString().equals(saltstring)) {
		       throw new ControllerException(SERVICEEXCEPTIONURLMISSMATCH); 
	     }
		if( !userentity.isUserIsSeller()) {
			throw new ServiceException(SERVICEXCEPTIONUSERNOTSELLER);
		}
		
		PropertyEntity propertyToBeDeleted=propertyrepository.findPropertyByPropertyId(userProperty.getReturnPropertyId());
		if(!propertyToBeDeleted.isPropertyIsActive()) {
			throw new ServiceException(SERVICEEXCEPTIONPROPERYALREADYINACTIVE);
		}else {
			propertyToBeDeleted.setPropertyIsActive(false);
		}
		propertyrepository.save(propertyToBeDeleted);
		return SUCCESSMESSAGEFORDELETE;
	}catch(ServiceException exception) {
		logg(exception.getMessage());
		throw exception;
	}catch(Exception exception) {
		logg("DeletePropertyBySeller "+exception.getMessage());
		throw exception;
	}
	
}





	
	/**
	 * Image entity for property.
	 *
	 * @param propertyImageList the property image list
	 * @return the list
	 */
	public List<ImageEntity> imageEntityForProperty(List<Image> propertyImageList) {
		List<ImageEntity> imageentitylist=new ArrayList<>();
		try {	
			for (Image image1 : propertyImageList) {
			ImageEntity image= new ImageEntity();
			image.setImageCreatedTime(image1.getImageCreatedTime());
			image.setImageDescription(image1.getImageDescription());
			image.setImageId(image1.getImageId());
			image.setImageIsActive(image1.isImageIsActive());
			image.setImagePath(image1.getImagePath());
			imageentitylist.add(image);
			}
			return imageentitylist;
		}catch(Exception exception) {
			logg("imageForProperty "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Property from add propety.
	 *
	 * @param userproperty the userproperty
	 * @return the property
	 */
	public Property propertyFromAddPropety(AddPropertyUser userproperty) {
		try {
		Property p=new Property();
		p.setPropertyCent(userproperty.getPropertyCent());
		p.setPropertyCity(userproperty.getPropertyCity());
		p.setPropertyCountry(userproperty.getPropertyCountry());
		p.setPropertyDescription(userproperty.getPropertyDescription());
		p.setPropertyEsteematedAmount(userproperty.getPropertyEsteematedAmount());
		p.setPropertyImageIds(userproperty.getPropertyImageIds());
		p.setPropertyLandmark(userproperty.getPropertyLandmark());
		p.setPropertyLatitude(userproperty.getPropertyLatitude());
		p.setPropertyLongitude(userproperty.getPropertyLongitude());
		p.setPropertyOwnedBy(userproperty.getPropertyOwnedBy());
		p.setPropertyState(userproperty.getPropertyState());
		p.setPropertyType(userproperty.getPropertyType());
		
		return p;
		
		}catch(Exception exception) {
			logg("propertyForAddProperty "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Returnproperty.
	 *
	 * @param propertyentitylist the propertyentitylist
	 * @return the list
	 */
	public List<Property> returnproperty(List<PropertyEntity> propertyentitylist){
		try {
			List<Property> returnPropertyList=new ArrayList<>();
			for (PropertyEntity p1 : propertyentitylist) {
				if(p1.isPropertyIsActive()) {
					Property p=new Property();
					p.setPropertyCent(p1.getPropertyCent());
					p.setPropertyCity(p1.getPropertyCity());
					p.setPropertyCountry(p1.getPropertyCountry());
					p.setPropertyCreatedTime(p1.getPropertyCreatedTime());
					p.setPropertyDescription(p1.getPropertyDescription());
					p.setPropertyEsteematedAmount(p1.getPropertyEsteematedAmount());
				if(!p1.getPropertyFeedbackId().isEmpty()) {
					p.setPropertyFeedbackIds(this.feedbackForReturnEntity(p1.getPropertyFeedbackId()));
				}
				p.setPropertyId(p1.getPropertyId());
				if(!p1.getPropertyImageId().isEmpty()) {
					p.setPropertyImageIds(this.imageForReturnProperty(p1.getPropertyImageId()));
				}
				p.setPropertyIsActive(p1.isPropertyIsActive());
				p.setPropertyLandmark(p1.getPropertyLandmark());
				p.setPropertyLatitude(p1.getPropertyLatitude());
				p.setPropertyLongitude(p1.getPropertyLongitude());
				p.setPropertyModifiedTime(p1.getPropertyModifiedTime());
				p.setPropertyOwnedBy(p1.getPropertyOwnedBy());
				p.setPropertyState(p1.getPropertyState());
				p.setPropertyType(p1.getPropertyType());
				p.setPropertyViewedCount(p1.getPropertyViewedCount());
				if(!p1.getPropertyViewedId().isEmpty()) {
					p.setPropertyViewedIds(this.viewedproperty(p1.getPropertyViewedId()));
				}
				returnPropertyList.add(p);
			}
			}
			return returnPropertyList;
			
		}catch(Exception exception) {
			logg("returnProperty"+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Feedback for return entity.
	 *
	 * @param feedbackentityList the feedbackentity list
	 * @return the list
	 */
	public List<Feedback> feedbackForReturnEntity(List<FeedbackEntity> feedbackentityList){
		try {
			List<Feedback> feedbacklist=new ArrayList<>();
			for (FeedbackEntity feedback1 : feedbackentityList) {
				Feedback feedbackToAddToAdmin= new Feedback();
				feedbackToAddToAdmin.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
				feedbackToAddToAdmin.setFeedbackDescription(feedback1.getFeedbackDescription());
				feedbackToAddToAdmin.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
				feedbackToAddToAdmin.setFeedbackId(feedback1.getFeedbackId());
				feedbackToAddToAdmin.setFeedbackRating(feedback1.getFeedbackRating());
				feedbacklist.add(feedbackToAddToAdmin);
			}
			return feedbacklist;
		}catch(Exception exception) {
			logg("returnPropertyFeedbackList"+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Image for return property.
	 *
	 * @param imageList the image list
	 * @return the list
	 */
	public List<Image> imageForReturnProperty(List<ImageEntity> imageList){
		try {
			List<Image> imagelist=new ArrayList<>();
			for(ImageEntity image1 : imageList) {
				Image image=new Image();
				image.setImageCreatedTime(image1.getImageCreatedTime());
				image.setImageDescription(image1.getImageDescription());
				image.setImageId(image1.getImageId());
				image.setImageIsActive(image1.isImageIsActive());
				image.setImagePath(image1.getImagePath());
				imagelist.add(image);
			}
			return imagelist;
		}catch(Exception exception) {
			logg("imageForReturnProperty"+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Viewedproperty.
	 *
	 * @param viewedPropertyList the viewed property list
	 * @return the list
	 */
	public List<ViewedProperty> viewedproperty (List<ViewedPropertyEntity> viewedPropertyList){
		try {
		List<ViewedProperty> viewedPropertyOfSeller=new ArrayList<>();
		for (ViewedPropertyEntity viewedproperty : viewedPropertyList) {
			ViewedProperty viewproperty=new ViewedProperty();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(this.propertyForViewedProperty(viewedproperty.getViewedPropertyPropertyId()));
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfSeller.add(viewproperty);
		}
		return viewedPropertyOfSeller;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	} 
	
	public List<FeedbackEntity> feedbackEntityList(List<Feedback> feedbackList){
		try {
			List<FeedbackEntity> feedbacklist=new ArrayList<>();
			for (Feedback feedback1 : feedbackList) {
				FeedbackEntity feedbackToAdd= new FeedbackEntity();
				feedbackToAdd.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
				feedbackToAdd.setFeedbackDescription(feedback1.getFeedbackDescription());
				feedbackToAdd.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
				feedbackToAdd.setFeedbackId(feedback1.getFeedbackId());
				feedbackToAdd.setFeedbackRating(feedback1.getFeedbackRating());
				feedbacklist.add(feedbackToAdd);
			}
			return feedbacklist;
		}catch(Exception exception) {
			logg("returnPropertyFeedbackList"+exception.getMessage());
			throw exception;
		}
	}
	
	public List<ViewedPropertyEntity> viewedpropertyEntityList (List<ViewedProperty> viewedPropertyList){
		try {
		List<ViewedPropertyEntity> viewedPropertyEntity=new ArrayList<>();
		for (ViewedProperty viewedproperty : viewedPropertyList) {
			ViewedPropertyEntity viewproperty=new ViewedPropertyEntity();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(this.propertyEntityForViewedPropertyEntity(viewedproperty.getViewedPropertyPropertyId()));
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyEntity.add(viewproperty);
		}
		return viewedPropertyEntity;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	} 
	
	public Property propertyForViewedProperty(PropertyEntity propertyEntity){
		try {
			
			Property property=new Property();
			property.setPropertyCent(propertyEntity.getPropertyCent());
			property.setPropertyCity(propertyEntity.getPropertyCity());
			property.setPropertyCountry(propertyEntity.getPropertyCountry());
			property.setPropertyCreatedTime(propertyEntity.getPropertyCreatedTime());
			property.setPropertyDescription(propertyEntity.getPropertyDescription());
			property.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
			property.setPropertyId(propertyEntity.getPropertyId());
			property.setPropertyIsActive(propertyEntity.isPropertyIsActive());
			property.setPropertyLandmark(propertyEntity.getPropertyLandmark());
			property.setPropertyLatitude(propertyEntity.getPropertyLongitude());
			property.setPropertyLongitude(propertyEntity.getPropertyLongitude());
			property.setPropertyModifiedTime(propertyEntity.getPropertyModifiedTime());
			property.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
			property.setPropertyState(propertyEntity.getPropertyState());
			property.setPropertyType(propertyEntity.getPropertyType());
			property.setPropertyViewedCount(propertyEntity.getPropertyViewedCount());
			if(!propertyEntity.getPropertyFeedbackId().isEmpty()) {
				property.setPropertyFeedbackIds(this.feedbackForViewedProperty(propertyEntity.getPropertyFeedbackId()));
			}
			if(!propertyEntity.getPropertyImageId().isEmpty()) {
				property.setPropertyImageIds(this.imageForViewedProeprty(propertyEntity.getPropertyImageId()));
			}
			if(!propertyEntity.getPropertyViewedId().isEmpty()) {
				property.setPropertyViewedIds(this.viewedPropertyForViewedProperty(propertyEntity.getPropertyViewedId()));
			}
		return property;
		}catch(Exception exception) {
			logg("property "+exception.getMessage());
			throw exception;
		}
	}
	
	public List<Feedback> feedbackForViewedProperty(List<FeedbackEntity> feedbackentitylist){
		try {
		List<Feedback> feedbacklist=new ArrayList<>();
		for (FeedbackEntity feedback1 : feedbackentitylist) {
			Feedback feedbackToAddToProperty= new Feedback();
			feedbackToAddToProperty.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
			feedbackToAddToProperty.setFeedbackDescription(feedback1.getFeedbackDescription());
			feedbackToAddToProperty.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
			feedbackToAddToProperty.setFeedbackId(feedback1.getFeedbackId());
			feedbackToAddToProperty.setFeedbackRating(feedback1.getFeedbackRating());
			feedbacklist.add(feedbackToAddToProperty);
		}
		return feedbacklist;
		}catch(Exception exception) {
			logg("feedback "+exception.getMessage());
			throw exception;
		}
	}
	
	public List<Image> imageForViewedProeprty(List<ImageEntity> userImageEntity) {
		try {
			List<Image> imagelist=new ArrayList<>();
			for (ImageEntity image : userImageEntity) {
			Image image1= new Image();
			image1.setImageCreatedTime(image.getImageCreatedTime());
			image1.setImageDescription(image.getImageDescription());
			image1.setImageId(image.getImageId());
			image1.setImageIsActive(image.isImageIsActive());
			image1.setImagePath(image.getImagePath());
			imagelist.add(image1);
		}
			return imagelist;
		}catch(Exception exception) {
			logg("imageForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	public List<ViewedProperty> viewedPropertyForViewedProperty (List<ViewedPropertyEntity> viewedPropertyEntityList){
		try {
		List<ViewedProperty> viewedPropertyOfProperty=new ArrayList<>();
		for (ViewedPropertyEntity viewedproperty : viewedPropertyEntityList) {
			ViewedProperty viewproperty=new ViewedProperty();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(this.propertyForViewedProperty(viewedproperty.getViewedPropertyPropertyId()));
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfProperty.add(viewproperty);
		}
		return viewedPropertyOfProperty;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	}
	
	public PropertyEntity propertyEntityForViewedPropertyEntity(Property property){
		try {
			
			PropertyEntity propertyentity=new PropertyEntity();
			propertyentity.setPropertyCent(property.getPropertyCent());
			propertyentity.setPropertyCity(property.getPropertyCity());
			propertyentity.setPropertyCountry(property.getPropertyCountry());
			propertyentity.setPropertyCreatedTime(property.getPropertyCreatedTime());
			propertyentity.setPropertyDescription(property.getPropertyDescription());
			propertyentity.setPropertyEsteematedAmount(property.getPropertyEsteematedAmount());
			propertyentity.setPropertyId(property.getPropertyId());
			propertyentity.setPropertyIsActive(property.isPropertyIsActive());
			propertyentity.setPropertyLandmark(property.getPropertyLandmark());
			propertyentity.setPropertyLatitude(property.getPropertyLongitude());
			propertyentity.setPropertyLongitude(property.getPropertyLongitude());
			propertyentity.setPropertyModifiedTime(property.getPropertyModifiedTime());
			propertyentity.setPropertyOwnedBy(property.getPropertyOwnedBy());
			propertyentity.setPropertyState(property.getPropertyState());
			propertyentity.setPropertyType(property.getPropertyType());
			propertyentity.setPropertyViewedCount(property.getPropertyViewedCount());
			if(!property.getPropertyFeedbackIds().isEmpty()) {
				propertyentity.setPropertyFeedbackId(this.feedbackEntityForViewedPropertyEntity(property.getPropertyFeedbackIds()));
			}
			if(!property.getPropertyImageIds().isEmpty()) {
				propertyentity.setPropertyImageId(this.imageEntiyForViewedProeprtyEntity(property.getPropertyImageIds()));
			}
			if(!property.getPropertyViewedIds().isEmpty()) {
				propertyentity.setPropertyViewedId(this.viewedPropertyEntityForViewedPropertyEntity(property.getPropertyViewedIds()));
			}
		return propertyentity;
		}catch(Exception exception) {
			logg("property "+exception.getMessage());
			throw exception;
		}
	}
	
	public List<ViewedPropertyEntity> viewedPropertyEntityForViewedPropertyEntity (List<ViewedProperty> viewedPropertyList){
		try {
		List<ViewedPropertyEntity> viewedPropertyOfProperty=new ArrayList<>();
		for (ViewedProperty viewedproperty : viewedPropertyList) {
			ViewedPropertyEntity viewpropertyentity=new ViewedPropertyEntity();
			viewpropertyentity.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewpropertyentity.setViewedPropertyPropertyId(this.propertyEntityForViewedPropertyEntity(viewedproperty.getViewedPropertyPropertyId()));
			viewpropertyentity.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewpropertyentity.setViewedTime(viewedproperty.getViewedTime());
			viewpropertyentity.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfProperty.add(viewpropertyentity);
		}
		return viewedPropertyOfProperty;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	}
	
	public List<ImageEntity> imageEntiyForViewedProeprtyEntity(List<Image> userImage) {
		try {
			List<ImageEntity> imageEntiylist=new ArrayList<>();
			for (Image image : userImage) {
			ImageEntity imageentity= new ImageEntity();
			imageentity.setImageCreatedTime(image.getImageCreatedTime());
			imageentity.setImageDescription(image.getImageDescription());
			imageentity.setImageId(image.getImageId());
			imageentity.setImageIsActive(image.isImageIsActive());
			imageentity.setImagePath(image.getImagePath());
			imageEntiylist.add(imageentity);
		}
			return imageEntiylist;
		}catch(Exception exception) {
			logg("imageForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	public List<FeedbackEntity> feedbackEntityForViewedPropertyEntity(List<Feedback> feedbacklist){
		try {
		List<FeedbackEntity> feedbackEntitylist=new ArrayList<>();
		for (Feedback feedback1 : feedbacklist) {
			Feedback feedbackToAddToAdmin= new Feedback();
			feedbackToAddToAdmin.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
			feedbackToAddToAdmin.setFeedbackDescription(feedback1.getFeedbackDescription());
			feedbackToAddToAdmin.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
			feedbackToAddToAdmin.setFeedbackId(feedback1.getFeedbackId());
			feedbackToAddToAdmin.setFeedbackRating(feedback1.getFeedbackRating());
			feedbacklist.add(feedbackToAddToAdmin);
		}
		return feedbackEntitylist;
		}catch(Exception exception) {
			logg("feedback "+exception.getMessage());
			throw exception;
		}
	}
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}

}
