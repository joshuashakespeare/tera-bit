package com.project.terabit.service;

import com.project.terabit.model.CreateSeller;
import com.project.terabit.model.SaveSeller;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;


public interface SellerService {

	public Seller updateSeller(SaveSeller updateSeller,String saltString) throws Exception;

	public Seller createSeller(String saltstring,CreateSeller createseller) throws Exception;
	
	public User deleteSeller(User user,String saltString) throws Exception;

	}
