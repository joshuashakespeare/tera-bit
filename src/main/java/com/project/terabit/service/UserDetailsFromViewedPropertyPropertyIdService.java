package com.project.terabit.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.project.terabit.model.UserDetail;

@Service
public interface UserDetailsFromViewedPropertyPropertyIdService {
	
	public List<UserDetail> getUserDetailsForPropertyId(String saltstring, UserDetail incomminguserdetail) throws Exception;

}
