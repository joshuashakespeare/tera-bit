package com.project.terabit.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerImpl {

	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	public void logg(String message) {
		log.error(message);
	}
}
