package com.project.terabit.utility;

import java.util.ArrayList;



// TODO: Auto-generated Javadoc
/**
 * The Class PasswordHashing.
 */
public class PasswordHashing {
       
       /**
        * Hashing.
        *
        * @param password the password
        * @return the string
        * @throws Exception the exception
        */
       public String hashing(String password) throws Exception {
       try {
		       ArrayList<Character> individualcharacterlist=new ArrayList<>();
		       Integer hashcode=0;
		       Integer initialvalue=1;
		       ArrayList<Integer> powers= new ArrayList<>();
		       for(int j=0;j<password.length();j++) {
		              initialvalue=initialvalue*password.length();
		              powers.add(initialvalue);
		       }
		       for(int index=0;index<password.length();index++) {
		              Character individualcharacter=password.charAt(index);
		              individualcharacterlist.add(individualcharacter);    
		       }
		       for(int index=0;index<password.length();index++) {
		              if(index%2==0) {
		                     hashcode=hashcode+individualcharacterlist.get(password.length()-1-index).hashCode()*powers.get(password.length()-1-index)+(13*index)-(5*(index-1))+(7*(index+18));
		              }
		              if(index%2==1) {
		                     hashcode=hashcode+individualcharacterlist.get(password.length()-1-index).hashCode()*powers.get(password.length()-1-index)+(13*index)-(5*(index-1))+(7*(index+18));
		              }
		       }
		              return hashcode.toString();
       }catch(Exception exception) {
    	   throw exception;
       }
       }
	
}