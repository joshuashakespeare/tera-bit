package com.project.terabit.utility;

import java.util.Random;


// TODO: Auto-generated Javadoc
/**
 * The Class RandomStringGenerator.
 */
public class RandomStringGenerator {
	
	
	
	/**
	 * Random string.
	 *
	 * @return the string
	 */
	public String randomString() {
		StringBuilder bld = new StringBuilder();
		String saltstring="";
		int c=0;
		while(c<10) {
		Random rand=new Random();
		int randint=rand.nextInt(130);
		String string=Character.toString((char)randint);
		if(string.matches("[a-zA-Z0-9]")){
			c+=1;
			bld.append(string);
		}
		}
		saltstring=bld.toString();
		return saltstring;
	}
}
