package com.project.terabit.validator;

import java.math.BigInteger;

import com.project.terabit.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginUserValidator.
 */
public class LoginUserValidator {

	/**
	 * Instantiates a new login user validator.
	 */
	private LoginUserValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	/** The Constant mailIdException. */
	private static final String MAILIDEEXCEPTION = "VALIDATOR.invalid_email_id";
	
	/** The Constant phoneNoException. */
	private static final String PHONENUMBEREXCEPTION = "VALIDATOR.invalid_phone_no";
	
	/** The Constant passwordException. */
	private static final String PASSKEYEXCEPTION = "VALIDATOR.invalid_password";
	
	/**
	 * Validate.
	 *
	 * @param user the user
	 * @throws Exception the exception
	 */
	public static void validate(User user) throws Exception {
		if(user.getUserEmailId()!=null) {
			if(!validatemailId(user.getUserEmailId()))  throw new Exception(MAILIDEEXCEPTION);
		}
		if(user.getUserContactNo()!=null) {
		if(!validatePhoneNumber(user.getUserContactNo()))  throw new Exception(PHONENUMBEREXCEPTION);
		}
		if(!validatePassword(user.getUserPassword()))   throw new Exception(PASSKEYEXCEPTION);
	}
	
	/**
	 * Validatemail id.
	 *
	 * @param mailId the mail id
	 * @return the boolean
	 */
	public static Boolean validatemailId(String mailId) {
		Boolean flag=false;
		if(mailId.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" )) {
			flag=true;
		}return flag;
		
		
	}
	
	/**
	 * Validate phone number.
	 *
	 * @param phoneNo the phone no
	 * @return the boolean
	 */
	public static Boolean validatePhoneNumber(BigInteger phoneNo) {
		Boolean flag=false;
		if(phoneNo.toString().matches("[1-9][0-9]{9}")) {
			flag=true;
		}return flag;
	}
	
	/**
	 * Validate password.
	 *
	 * @param password the password
	 * @return the boolean
	 */
	public static Boolean validatePassword(String password) {
		Boolean flag=false;
		if(password.matches("(?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40}")) {
			flag=true;
		}return flag;
	}
}
