package com.project.terabit.validator;

import java.math.BigInteger;

import com.project.terabit.model.CreateSeller;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateSellerValidator.
 */
public class SellerValidator {
	
	
	/**
	 * Instantiates a new creates the seller validator.
	 */
	private SellerValidator() {
	    throw new IllegalStateException("Utility class");
	  }

	/** The Constant PHONENUMBEREXCEPTION. */
	private static final String PHONENUMBEREXCEPTION = "SELLERVALIDATOR.invalid_phone_no";
	
	/** The Constant USERIDEXCEPTION. */
	private static final String USERIDEXCEPTION = "SELLERVALIDATOR.invalid_user_id";

	/**
	 * Validate.
	 *
	 * @param createseller the createseller
	 * @throws Exception the exception
	 */
	public static void validate(CreateSeller createseller) throws Exception {
		if(!validateUserId(createseller.getUserId().toString()))  throw new Exception(USERIDEXCEPTION);
		if(!validatePrivateContactNumber(createseller.getSellerPrivateContact()))  throw new Exception(PHONENUMBEREXCEPTION);
	}
	
	/**
	 * Validate user id.
	 *
	 * @param userId the user id
	 * @return the boolean
	 */
	public static Boolean validateUserId(String userId) {
		Boolean flag = false;
		if(userId.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")) {
			flag = true;
		}return flag;
	}
	
	/**
	 * Validate private contact number.
	 *
	 * @param phoneNo the phone no
	 * @return the boolean
	 */
	public static Boolean validatePrivateContactNumber(BigInteger phoneNo) {
		Boolean flag=false;
		if(phoneNo.toString().matches("[6-9][0-9]{9}") && (!phoneNo.toString().matches("[7]{10}")) 
				&& (!phoneNo.toString().matches("[6]{10}"))
				&& (!phoneNo.toString().matches("[8]{10}")) && (!phoneNo.toString().matches("[9]{10}"))) {
			flag=true;
		}return flag;
	}
}
