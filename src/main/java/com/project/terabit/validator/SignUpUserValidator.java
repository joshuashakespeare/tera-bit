package com.project.terabit.validator;

import java.math.BigInteger;

import com.project.terabit.model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class SignUpValidator.
 */
public class SignUpUserValidator {
	
	/**
	 * Instantiates a new sign up user validator.
	 */
	private SignUpUserValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	/** The Constant mailIdException. */
	private static final String MAILIDEEXCEPTION = "SIGNUPVALIDATOR.invalid_email_id";
	
	/** The Constant phoneNoException. */
	private static final String PHONENUMBEREXCEPTION = "SIGNUPVALIDATOR.invalid_phone_no";
	
	/** The Constant passwordException. */
	private static final String PASSKEYEXCEPTION = "SIGNUPVALIDATOR.invalid_password";
	
	private static final String FIRSTNAMEEXCEPTION = "SIGNUPVALIDATOR.invalid_firstname";
	private static final String LASTNAMEEXCEPTION = "SIGNUPVALIDATOR.invalid_lastname";
	
	/**
	 * Validate.
	 *
	 * @param user the user
	 * @throws Exception the exception
	 */
	public static void validate(User user) throws Exception {
		if(!validateFirstName(user.getUserFirstName())) throw new Exception(FIRSTNAMEEXCEPTION);
		if(!validateLastName(user.getUserLastName())) throw new Exception(LASTNAMEEXCEPTION);
		if(!validatemailId(user.getUserEmailId()))  throw new Exception(MAILIDEEXCEPTION);
		if(!validatePhoneNumber(user.getUserContactNo()))  throw new Exception(PHONENUMBEREXCEPTION);
		if(!validatePassword(user.getUserPassword()))   throw new Exception(PASSKEYEXCEPTION);
	}
	
	
	
	/**
	 * Validatemail id.
	 *
	 * @param mailId the mail id
	 * @return the boolean
	 */
	
	public static Boolean validateFirstName(String firstName) {
		boolean flag = false;
		if(firstName.matches("[A-Za-z][a-z]*")) {
			flag = true;
		}return flag;
	}
	public static Boolean validateLastName(String lastName) {
		boolean flag = false;
		if(lastName.matches("[A-Za-z]+")) {
			flag = true;
		}return flag;
	}
	public static Boolean validatemailId(String mailId) {
		boolean flag=false;
		if(mailId.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" )) {
			flag=true;
		}
		return flag;
		
		
	}
	
	/**
	 * Validate phone number.
	 *
	 * @param phoneNo the phone no
	 * @return the boolean
	 */
	public static Boolean validatePhoneNumber(BigInteger phoneNo) {
		boolean flag=false;
		if(phoneNo.toString().matches("[7-9][0-9]{9}") && (!phoneNo.toString().matches("[7]{10}")) 
				&& (!phoneNo.toString().matches("[8]{10}")) && (!phoneNo.toString().matches("[9]{10}"))) {
			flag=true;
		}return flag;
	}
	
	/**
	 * Validate password.
	 *
	 * @param password the password
	 * @return the boolean
	 */
	public static Boolean validatePassword(String password) {
		boolean flag=false;
		if(password.matches("(?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40}")) {
			flag=true;
		}return flag;
	}

}
