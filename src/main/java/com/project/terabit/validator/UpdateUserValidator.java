package com.project.terabit.validator;

import java.math.BigInteger;

import com.project.terabit.model.User;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateValidator.
 */
public class UpdateUserValidator {
	
	/**
	 * Instantiates a new update user validator.
	 */
	private UpdateUserValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	/** The Constant MAILIDEEXCEPTION. */
	private static final String MAILIDEEXCEPTION = "UPDATEUSERVALIDATOR.invalid_email_id";
	
	/** The Constant PHONENUMBEREXCEPTION. */
	private static final String PHONENUMBEREXCEPTION = "UPDATEUSERVALIDATOR.invalid_phone_no";
	
	/**
	 * Update info validate.
	 *
	 * @param user the user
	 * @throws Exception the exception
	 */
	public static void updateInfoValidate(User user) throws Exception {
		if(!validatemailId(user.getUserEmailId()))  throw new Exception(MAILIDEEXCEPTION);
		if(!validatePhoneNumber(user.getUserContactNo()))  throw new Exception(PHONENUMBEREXCEPTION);
	}
	
	/**
	 * Validatemail id.
	 *
	 * @param mailId the mail id
	 * @return the boolean
	 */
	public static Boolean validatemailId(String mailId) {
		boolean flag=false;
		if(mailId.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" )) {
			flag=true;
		}return flag;
		
		
	}
	
	/**
	 * Validate phone number.
	 *
	 * @param phoneNo the phone no
	 * @return the boolean
	 */
	public static Boolean validatePhoneNumber(BigInteger phoneNo) {
		boolean flag=false;
		if(phoneNo.toString().matches("[6-9][0-9]{9}") && (!phoneNo.toString().matches("[7]{10}")) 
				&& (!phoneNo.toString().matches("[6]{10}"))
				&& (!phoneNo.toString().matches("[8]{10}")) && (!phoneNo.toString().matches("[9]{10}"))) {
			flag=true;
		}return flag;
	}

}
