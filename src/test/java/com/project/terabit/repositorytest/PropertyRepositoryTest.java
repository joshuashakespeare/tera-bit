//package com.project.terabit.repositorytest;
//
//import java.math.BigInteger;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//
//import com.project.terabit.TerabitApplicationTests;
//import com.project.terabit.entity.PropertyEntity;
//import com.project.terabit.repositroy.PropertyRepository;
//import com.project.terabit.service.HomePagePropertyDisplayImpl;
//
//// TODO: Auto-generated Javadoc
///**
// * The Class PropertyRepositoryTest.
// */
//public class PropertyRepositoryTest extends TerabitApplicationTests {
//	
//	/** The propertyrepository. */
//	@Mock
//	PropertyRepository propertyrepository;
//	
//	/** The service. */
//	@InjectMocks
//	HomePagePropertyDisplayImpl service;
//	
//	/** The expected exception. */
//	@Rule
//    public ExpectedException expectedException = ExpectedException.none();
//	
//	@Test
//	public void emptyListException() throws Exception {	
//		expectedException.expect(Exception.class);
//        expectedException.expectMessage("SERVICEEXCEPTION.Empty_list_returned");
//        Mockito.when(propertyrepository.getTotalPropertyCount()).thenReturn(null);
//        BigInteger lastpropertyid=BigInteger.valueOf(1);
//        service.propertyDisplay(lastpropertyid);
//	}
//	
//	@Test
//	public void totalcountException() throws Exception {	
//		expectedException.expect(Exception.class);
//        expectedException.expectMessage("SERVICEEXCEPTION.Count_returned_null");
//        BigInteger lastpropertyid=BigInteger.valueOf(1);
//        List<PropertyEntity> propertyentityemptylist=new ArrayList<>();
//        Mockito.when(propertyrepository.getpropertyForHomePage(lastpropertyid)).thenReturn(propertyentityemptylist);
//        service.propertyDisplay(lastpropertyid);
//	}
//
//}
