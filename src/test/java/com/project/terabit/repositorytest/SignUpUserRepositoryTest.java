//package com.project.terabit.repositorytest;
//
//import java.math.BigInteger;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//
//import com.project.terabit.TerabitApplicationTests;
//import com.project.terabit.entity.UsersEntity;
//import com.project.terabit.model.User;
//import com.project.terabit.repositroy.UserRepository;
//import com.project.terabit.service.SignUpUserServiceImpl;
//import com.project.terabit.utility.PasswordHashing;
//
//// TODO: Auto-generated Javadoc
///**
// * The Class SignUpUserRepositoryTest.
// */
//public class SignUpUserRepositoryTest extends TerabitApplicationTests {
//	
//	/** The userrepository. */
//	@Mock
//	UserRepository userrepository;
//	
//	/** The signupservice. */
//	@InjectMocks
//	SignUpUserServiceImpl signupservice;
//	
//	/** The password hasher. */
//	@InjectMocks
//	PasswordHashing passwordHasher;
//	
//    /** The expected exception. */
//    @Rule
//    public ExpectedException expectedException = ExpectedException.none();
//    
//    /**
//     * Check user exception.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public  void checkUserException() throws Exception{
//    	expectedException.expect(Exception.class);
//        expectedException.expectMessage("SERVICE.invalid_signUp");
//        UsersEntity userentity = new UsersEntity();
//        User user = new User();
//        user.setUserEmailId("lisadeepik@gmail.com");
//        user.setUserContactNo(BigInteger.valueOf(7639312932l));
//        user.setUserPassword("Deepik@lisa79");
//        Mockito.when(userrepository.checkUser("lisadeepik@gmail.com", BigInteger.valueOf(7639312932l))).thenReturn(userentity);
//        signupservice.saveUser(user);     
//    }
//   
//    /**
//     * Check user success.
//     *
//     * @throws Exception the exception
//     */
//    @Test
//    public void checkUserSuccess() throws Exception{
//        User user = new User();
//        user.setUserFirstName("lisa");
//        user.setUserLastName("Deepik");
//        user.setUserEmailId("lisadeepik@gmail.com");
//        user.setUserContactNo(BigInteger.valueOf(7639312932l));
//        user.setUserPassword("Deepik@lisa79");
//        Mockito.when(userrepository.checkUser("lisadeepik@gmail.com", BigInteger.valueOf(7639312932l))).thenReturn(null);
//        signupservice.saveUser(user); 
//    }
//
//}
