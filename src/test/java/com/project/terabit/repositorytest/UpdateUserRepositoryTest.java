//package com.project.terabit.repositorytest;
//
//import org.junit.Rule;
//import org.junit.rules.ExpectedException;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.project.terabit.TerabitApplicationTests;
//import com.project.terabit.repositroy.UserRepository;
//import com.project.terabit.service.SignUpUserServiceImpl;
//
//// TODO: Auto-generated Javadoc
///**
// * The Class UpdateUserRepositoryTest.
// */
//public class UpdateUserRepositoryTest extends TerabitApplicationTests {
//	
//	/** The userrepository. */
//	@Mock
//	UserRepository userrepository;
//	
//	/** The signupservice. */
//	@Autowired
//	SignUpUserServiceImpl signupservice;
//	
//	/** The expected exception. */
//	@Rule 
//	public ExpectedException expectedException=ExpectedException.none();
//	
////	@Test
////	public void updateServiceNoUserException() throws Exception{
////		expectedException.expect(Exception.class);
////		expectedException.expectMessage("UPDATESERVICE.invalid_uuid");
////		User userToBeUpdated = new User();
////		userToBeUpdated.setUserEmailId("joshuashakespeare99@gmail.com");
////		userToBeUpdated.setUserContactNo(BigInteger.valueOf(7598443130l));
////		userToBeUpdated.setUserId(UUID.fromString("64117d20-448e-4686-bf5e-72324799bb76"));
////		System.out.println(userToBeUpdated.getUserId());
////		Mockito.when(userrepository.findUserByUserId(userToBeUpdated.getUserId())).thenReturn(null);
////		signupservice.updateUser(userToBeUpdated);
////	}
//	
////	@Test
////	public void updateServiceSuccess() throws Exception{
////		User userToBeUpdated = new User();
////		UserEntity userentity=new UserEntity();
////		userToBeUpdated.setUserFirstName("joshua");
////		userToBeUpdated.setUserLastName("shakespeare");
////		userToBeUpdated.setUserIsActive(true);
////		userToBeUpdated.setUserPassword("1243643");
////		userToBeUpdated.setUserCreatedTime(LocalDateTime.now());
////		userToBeUpdated.setUserModifiedTime(LocalDateTime.now());
////		userToBeUpdated.setUserId(UUID.fromString("71a7ca72-a1e2-4daa-b120-b91031cb8c24"));
////		userToBeUpdated.setUserEmailId("joshuashakespeare99@gmail.com");
////		userToBeUpdated.setUserContactNo(BigInteger.valueOf(7598443130l));
////		System.out.println(userToBeUpdated.getUserId());
////		Mockito.when(userrepository.findUserByUserId(userToBeUpdated.getUserId())).thenReturn(userentity);
////		signupservice.updateUser(userToBeUpdated);
////	}
//}
