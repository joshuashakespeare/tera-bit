//package com.project.terabit.service;
//
//import java.math.BigInteger;
//import java.util.UUID;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//
//import com.project.terabit.TerabitApplicationTests;
//import com.project.terabit.entity.SellerEntity;
//import com.project.terabit.entity.UsersEntity;
//import com.project.terabit.model.CreateSeller;
//import com.project.terabit.repository.SellerRepository;
//import com.project.terabit.repository.UserRepository;
//
//public class CreateSellerServiceTest extends TerabitApplicationTests{
//	
//	@Mock 
//	UserRepository userRepository;
//	
//	@Mock 
//	SellerRepository sellerRepository;
//	
//	@InjectMocks
//	SellerServiceImpl service;
//
//	@Rule
//	public ExpectedException expectedException = ExpectedException.none();
//
//	@Test
//	public void createSellerInvalidUserTest() throws Exception{
//		expectedException.expect(Exception.class);
//		expectedException.expectMessage("CREATESELLERSERVICE.No_User_Exists");
//		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
//		String saltstring = null;
//		CreateSeller createseller = new CreateSeller();
//		createseller.setUserId(UUID.fromString(uuid));
//		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
//		Mockito.when(userRepository.findUserByUserId(UUID.fromString(uuid))).thenReturn(null);
//		System.out.println(UUID.fromString(uuid));
//		service.createSeller(saltstring, createseller);
//	}
//	
//	@Test
//	public void createSellerSuccess() throws Exception{
//		String saltstring = null;
//		CreateSeller createseller = new CreateSeller();
//		String uuid = "8705b317-3bda-4ef7-abf7-2d951edc4ee7";
//		createseller.setUserId(UUID.fromString(uuid));
//		createseller.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
//		UsersEntity userEntity = new UsersEntity();
//		userEntity.setUserId(UUID.fromString(uuid));
//		Mockito.when(userRepository.findUserByUserId(createseller.getUserId())).thenReturn(userEntity);
//		service.createSeller(saltstring, createseller);
//		}
//	@Test
//	public void createSellerInvalidContactNo() throws Exception{
//		expectedException.expect(Exception.class);
//		expectedException.expectMessage("CREATESELLERSERVICE.ContactNo_already_exists");
//		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
//		String saltstring = null;
//		CreateSeller createseller = new CreateSeller();
//		createseller.setUserId(UUID.fromString(uuid));
//		createseller.setSellerPrivateContact(BigInteger.valueOf(999999999L));
//		SellerEntity sellerEntity = new SellerEntity();
//		sellerEntity.setSellerPrivateContact(BigInteger.valueOf(9999999999L));
//		Mockito.when(sellerRepository.findSellerByContactNumber(BigInteger.valueOf(9999999999L))).thenReturn(sellerEntity);
//		service.createSeller(saltstring, createseller);
//		}
//}
