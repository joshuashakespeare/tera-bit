//package com.project.terabit.service;
//
//import java.math.BigInteger;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//
//import com.project.terabit.TerabitApplicationTests;
//import com.project.terabit.entity.PropertyEntity;
//import com.project.terabit.repository.PropertyRepository;
//
//public class HomePagePropertyDisplayServiceTest extends TerabitApplicationTests{
//
//	@Mock 
//	PropertyRepository repository;
//
//	@InjectMocks
//	HomePagePropertyDisplayImpl service;
//
//	@Rule
//	public ExpectedException expectedException = ExpectedException.none();
//	@Test
//	public void negativePropertyIdException() throws Exception {
//		expectedException.expect(Exception.class);
//		expectedException.expectMessage("SERVICEEXCEPTION.last_property_id_is_negative");
//		BigInteger lastpropertyid=BigInteger.valueOf(-1);
//		service.propertyDisplay(lastpropertyid);
//	}
//	@Test
//	public void propertySuccess() throws Exception{
//		BigInteger lastpropertyid=BigInteger.valueOf(1);
//		Mockito.when(repository.getTotalPropertyCount()).thenReturn(BigInteger.valueOf(10));
//		List<PropertyEntity> propertyentitylist=new ArrayList<>();
//		PropertyEntity p=new PropertyEntity();
//		p.setPropertyCent(BigInteger.valueOf(10));
//		p.setPropertyCity("Chennai");
//		p.setPropertyCountry("India");
//		p.setPropertyCreatedTime(LocalDateTime.now());
//		p.setPropertyDescription("NA");
//		p.setPropertyEsteematedAmount("12222222");
//		p.setPropertyId(BigInteger.valueOf(114));
//		p.setPropertyIsActive(true);
//		p.setPropertyLandmark("NA");
//		p.setPropertyLatitude("NA");
//		p.setPropertyLongitude("NA");
//		p.setPropertyModifiedTime(LocalDateTime.now());
//		p.setPropertyOwnedBy(1);
//		p.setPropertyState("TamilNadu");
//		p.setPropertyViewedCount(BigInteger.valueOf(0));
//		propertyentitylist.add(p);
//		Mockito.when(repository.getpropertyForHomePage(lastpropertyid)).thenReturn(propertyentitylist);
//		service.propertyDisplay(lastpropertyid);
//	}
//
//}
