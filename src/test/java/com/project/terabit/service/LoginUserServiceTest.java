//package com.project.terabit.service;
//
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//
//import com.project.terabit.TerabitApplicationTests;
//import com.project.terabit.entity.UsersEntity;
//import com.project.terabit.model.User;
//import com.project.terabit.repository.UserRepository;
//
//public class LoginUserServiceTest extends TerabitApplicationTests {
//	
//	@Mock
//	UserRepository userrepository;
//	
//	@InjectMocks	
//	LoginUserServiceImpl loginservice;
//	
//	 @Rule
//	 public ExpectedException expectedException = ExpectedException.none();
//	 
//	 @Test
//	 public void userNotActiveException() throws Exception{
//		 expectedException.expect(Exception.class);
//	     expectedException.expectMessage("SERVICE.User_Not_Active");
//	     User user = new User();
//	     user.setUserEmailId("lisadeepik@gmail.com");
//	     user.setUserPassword("Deepik@lisa79");
//	     user.setUserIsActive(true);
//	     UsersEntity userentity=new UsersEntity();
//         userentity.setUserEmailId("lisadeepik@gmail.com");
//         userentity.setUserPassword("-1165077272");
//         userentity.setUserIsActive(false);
//         Mockito.when(userrepository.findUserByEmailID("lisadeepik@gmail.com")).thenReturn(userentity);
//        loginservice.login(user);   
//	 }
//	 
//	 @Test
//	public void testUserSuccess() throws Exception{
//	     User user = new User();
//	     user.setUserEmailId("lisadeepik@gmail.com");
//	     user.setUserPassword("Deepik@lisa79");
//	     user.setUserIsActive(true);
//	     UsersEntity userentity=new UsersEntity();
//         userentity.setUserEmailId("lisadeepik@gmail.com");
//         userentity.setUserPassword("-1165077272");
//         userentity.setUserIsActive(true);
//         Mockito.when(userrepository.findUserByEmailID("lisadeepik@gmail.com")).thenReturn(userentity);
//        loginservice.login(user);
//		}
//	 
//	
//	
//}
